package com.pluribus

import android.app.Application
import android.os.Build
import com.pluribus.BuildConfig.DEBUG
import com.pluribus.domain.interactors.LogInteractor
import com.pluribus.presentation.kodein
import com.pluribus.tools.logging.FileWriterTree
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import org.kodein.di.generic.instance
import timber.log.Timber

class PluribusApplication : Application() {

    companion object {
        lateinit var application: PluribusApplication
    }

    private val logInteractor by kodein.instance<LogInteractor>()
    private val disposables by lazy { CompositeDisposable() }

    override fun onCreate() {
        super.onCreate()
        application = this

        val config = YandexMetricaConfig.newConfigBuilder("7b0df7ee-8340-4265-b33e-c79ffa39b51a").build()
        YandexMetrica.activate(applicationContext, config)
        if (DEBUG) {
            // Initializes Timber DebugTree for Logcat event logging
            Timber.plant(Timber.DebugTree())
        }
        // Initializes Timber FileWriterTree for file event logging
        Timber.plant(FileWriterTree())
        Timber.d("Application has been created successfully")

        val map = linkedMapOf(
                "VERSION.SDK_INT" to Build.VERSION.SDK_INT,
                "SUPPORTED_ABIS" to Build.SUPPORTED_ABIS,
                "PRODUCT" to Build.PRODUCT,
                "MODEL" to Build.MODEL,
                "BRAND" to Build.BRAND,
                "ID" to Build.ID,
                "DISPLAY" to Build.DISPLAY,
                "DEVICE" to Build.DEVICE,
                "MANUFACTURER" to Build.MANUFACTURER,
                "SUPPORTED_32_BIT_ABIS" to Build.SUPPORTED_32_BIT_ABIS,
                "SUPPORTED_64_BIT_ABIS" to Build.SUPPORTED_64_BIT_ABIS,
                "VERSION.BASE_OS" to Build.VERSION.BASE_OS
        )

        Timber.i("device info ${JSONObject(map as Map<*, *>)}")

        val logsDisposable = logInteractor.uploadLogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.i("Logs have been uploaded successfully")
                }, { error ->
                    Timber.e(error.localizedMessage)
                })
        disposables.add(logsDisposable)
    }

    override fun onTerminate() {
        super.onTerminate()
        disposables.clear()
    }
}