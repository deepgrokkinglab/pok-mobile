package com.pluribus.tools

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.pluribus.presentation.record.LabelsService
import timber.log.Timber

class SwipeRecentDetectorService: Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }
   override fun onTaskRemoved(rootIntent: Intent) {
        Timber.e("Muh...App has been swiped from recents")
        LabelsService.stopService(this)
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("${this.javaClass.simpleName} has been destroyed")
    }
}
