package com.pluribus.tools

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import java.util.*

fun setContextLocale(context: Context, language: String): Context {
    val locale = Locale(language)
    val newCtx = updateResources(context, locale)
    val appCtx = context.applicationContext
    if (appCtx != null && context !== appCtx) {
        updateResources(appCtx, locale)
    }
    return newCtx
}

private fun updateResources(context: Context, locale: Locale): Context {
    Locale.setDefault(locale)
    val current = context
        .resources
        .configuration
        .getLocaleCompat()

    if (current == locale) {
        return context
    }
    updateResourcesLocaleLegacy(context, locale)
    @Suppress("UnnecessaryVariable")
    val newContext = context
    return newContext
}

@Suppress("DEPRECATION")
private fun updateResourcesLocaleLegacy(context: Context, locale: Locale): Context {
    val resources = context.resources
    val configuration = resources.configuration
    configuration.setLocale(locale)
    resources.updateConfiguration(configuration, resources.displayMetrics)
    return context
}

@Suppress("DEPRECATION")
private fun Configuration.getLocaleCompat(): Locale {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) locales.get(0) else locale
}
