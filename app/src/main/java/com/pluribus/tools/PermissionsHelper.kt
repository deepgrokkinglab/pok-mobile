package com.pluribus.tools

import android.Manifest
import android.content.Context
import android.os.Build
import android.provider.Settings
import androidx.core.content.PermissionChecker


object PermissionsHelper {

    fun checkOverlayPermission(applicationContext: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            Settings.canDrawOverlays(applicationContext)
        } else {
            PermissionChecker.PERMISSION_GRANTED == PermissionChecker.checkSelfPermission(
                applicationContext,
                Manifest.permission.SYSTEM_ALERT_WINDOW
            )
        }
    }

}