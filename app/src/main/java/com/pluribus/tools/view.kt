package com.pluribus.tools

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.pluribus.presentation.base.VerticalImageSpan

fun View.goneIf(gone: Boolean) {
    this.visibility = if (gone) View.GONE else View.VISIBLE
}

fun View.invisibleIf(invisible: Boolean) {
    this.visibility = if (invisible) View.INVISIBLE else View.VISIBLE
}

fun TextView.addImage(atText: String, @DrawableRes imgSrc: Int, imgWidth: Int, imgHeight: Int) {
    val ssb = SpannableStringBuilder(this.text)
    val drawable = ContextCompat.getDrawable(this.context, imgSrc) ?: return
    drawable.mutate()
    drawable.setBounds(0, 0,
            imgWidth,
            imgHeight)
    val start = text.indexOf(atText)
    ssb.setSpan(VerticalImageSpan(drawable), start, start + atText.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
    this.setText(ssb, TextView.BufferType.SPANNABLE)
}

fun TextView.setColor(fulltext: String, subtext: String, color: Int) {
    this.setText(fulltext, TextView.BufferType.SPANNABLE)
    val str = this.text as Spannable
    val i = fulltext.indexOf(subtext)
    str.setSpan(ForegroundColorSpan(color), i, i + subtext.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
}

fun BottomNavigationView.setSingleItemIconSizeRes(itemIndex: Int, @DimenRes sizeRes: Int) {
    val menuView = getChildAt(0) as? BottomNavigationMenuView ?: return
    val iconView = menuView.getChildAt(itemIndex)
            ?.findViewById<View>(com.google.android.material.R.id.icon) ?: return
    val iconSize = context.resources.getDimensionPixelSize(sizeRes)
    iconView.updateLayoutParams<ViewGroup.LayoutParams> {
        height = iconSize
        width = iconSize
    }
}
