package com.pluribus.tools

import com.pluribus.BuildConfig


fun compareCurrentAppVersionTo(anotherVersion: String): Boolean {
    if (BuildConfig.VERSION_CODE == 1) {
        return false
    }
    val b = anotherVersion.split('.')
    val otherCode = b.joinToString().toIntOrNull() ?: return false
    return BuildConfig.VERSION_CODE < otherCode
}