package com.pluribus.tools

fun Boolean.toInt() = if (this) 1 else 0
