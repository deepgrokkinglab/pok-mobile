package com.pluribus.tools

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel

object QRGenerator {

    fun generateBitmap(
        context: Context,
        margin: Int,
        content: String
    ): Bitmap? {
        val errorCorrectionLevel: ErrorCorrectionLevel = ErrorCorrectionLevel.Q
        val width: Int = (context.resources.displayMetrics.widthPixels / 1.3).toInt()
        val height: Int = (context.resources.displayMetrics.heightPixels / 2.4).toInt()

        val hintsMap = mapOf(
            EncodeHintType.CHARACTER_SET to "utf-8",
            EncodeHintType.ERROR_CORRECTION to errorCorrectionLevel,
            EncodeHintType.MARGIN to margin
        )

        try {
            val bitMatrix =
                QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hintsMap)
            val pixels = IntArray(width * height)
            for (i in 0 until height) {
                for (j in 0 until width) {
                    if (bitMatrix.get(j, i)) {
                        pixels[i * width + j] = Color.BLACK
                    } else {
                        pixels[i * width + j] = Color.WHITE
                    }
                }
            }
            return Bitmap.createBitmap(pixels, width, height, Bitmap.Config.ARGB_8888)
        } catch (ex: WriterException) {
        }
        return null
    }
}