package com.pluribus.tools

import android.util.Patterns
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.pluribus.R

fun validatePhone(editText: EditText, layout: TextInputLayout? = null): Boolean {
    val context = editText.context
    val str = editText.text.toString()
    val min = context.resources.getInteger(R.integer.phone_min_length)
    val max = context.resources.getInteger(R.integer.phone_max_length)
    val resources = editText.resources
    val error = if (!str.startsWith("+")) {
        resources.getString(R.string.phoneValidationErr_PhoneNumberMustBeginWithPlus)
    } else if (str.substring(1).any { !it.isDigit() }) {
        resources.getString(R.string.phoneValidationErr_PhoneNumberMustContainPlusAndDigitsOnly)
    } else if (str.length < min) {
        resources.getString(R.string.phoneValidationErr_PhoneNumberCanNotBeLessThan11Digits)
    } else if (str.length > max) {
        resources.getString(R.string.phoneValidationErr_PhoneNumberCanNotBeLongerThan16Digits)
    } else {
        null
    }
    if (layout != null) {
        layout.error = error
    } else {
        editText.error = error
    }
    return error == null
}

fun validateEmail(editText: EditText, layout: TextInputLayout? = null): Boolean {
    val context = editText.context
    val str = editText.text.toString()
    val min = context.resources.getInteger(R.integer.email_min_length)
    val max = context.resources.getInteger(R.integer.email_max_length)
    val resources = editText.resources
    var error: String?

    error = if (str.length < min) {
        resources.getString(R.string.emailValidationErr_EmailCanNotBeShorterThan5Digits)
    } else if (str.length > max) {
        resources.getString(R.string.emailValidationErr_EmailCanNotBeLongerThan48Digits)
    } else if (!Patterns.EMAIL_ADDRESS.matcher(str).matches()) {
        resources.getString(R.string.emailValidationErr_TheInputIsNotAValidEmailAddress)
    } else {
        null
    }

    if (layout != null) {
        layout.error = error
    } else {
        editText.error = error
    }
    return error == null
}

fun validatePassword(editText: EditText, layout: TextInputLayout? = null): Boolean {
    val context = editText.context
    val str = editText.text.toString()
    val min = context.resources.getInteger(R.integer.password_min_length)
    val max = context.resources.getInteger(R.integer.password_max_length)
    val resources = editText.resources
    val error: String?
    error = when {
        str.length < min -> {
            resources.getString(R.string.passwordValidationErr_PasswordCanNotBeLessThan6Characters)
        }
        str.length > max -> {
            resources.getString(R.string.passwordValidationErr_PasswordCanNotBeLongerThan32Characters)
        }
        else -> {
            null
        }
    }
    if (layout != null) {
        layout.error = error
    } else {
        editText.error = error
    }
    return error == null
}



