package com.pluribus.tools.logging

import com.pluribus.tools.files.FileHelper
import timber.log.Timber
import java.io.BufferedWriter
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*

class FileWriterTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        try {
            val logTimeStamp = SimpleDateFormat("MMM dd yyyy hh:mm:ss:SSS Z",
                    Locale.getDefault()).format(Date())
            val file = FileHelper.generateFile("logfile.txt")
            val writer = BufferedWriter(FileWriter(file, true))
            writer.run {
                append(logTimeStamp)
                tag?.let { append(it) }
                t?.let { append(it.localizedMessage) }
                append(" $message")
                newLine()
                flush()
                close()
            }
        } catch (exception: Exception) {
            println("Unable to log the log due to an error: ${exception.localizedMessage}")
        }

    }
}