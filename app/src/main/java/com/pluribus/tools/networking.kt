package com.pluribus.tools

import com.pluribus.presentation.main.settings.ServerData
import io.reactivex.Single
import timber.log.Timber
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.net.URL

@Throws(IOException::class)
fun URL.countPingToResource(): Int {
    try {
        val hostAddress: String = InetAddress.getByName(this.host).hostAddress
        val dnsResolved = System.currentTimeMillis()
        val socket = Socket(hostAddress, 80)
        socket.close()
        val probeFinish = System.currentTimeMillis()
        return (probeFinish - dnsResolved).toInt()
    } catch (ex: Exception) {
        Timber.e("ServerRepositoryImpl: Unable to ping: $ex")
    }
    return -1
}