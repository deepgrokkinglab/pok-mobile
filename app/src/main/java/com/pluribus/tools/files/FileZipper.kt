package com.pluribus.tools.files

import timber.log.Timber
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

object FileZipper {

    // Puts files into existing .zip file
    fun zipFiles(paths: Array<String>, /* Requires absolute path to the file */zipFileName: String) {
        val bufferSize = 65536
        try {
            val dest = FileOutputStream(zipFileName)
            val out = ZipOutputStream(BufferedOutputStream(dest))
            val data = ByteArray(bufferSize)
            for (path in paths) {
                val fi = FileInputStream(path)
                val origin = BufferedInputStream(fi, bufferSize)
                val entry = ZipEntry(path.substring(path.lastIndexOf(File.separator) + 1))
                out.putNextEntry(entry)
                while (true) {
                    val count = origin.read(data, 0, bufferSize)
                    if (count < 0) {
                        break
                    }
                    out.write(data, 0, count)
                }
                origin.close()
            }
            out.close()
        } catch (ex: Exception) {
            Timber.e(ex.localizedMessage)
        }
    }
}