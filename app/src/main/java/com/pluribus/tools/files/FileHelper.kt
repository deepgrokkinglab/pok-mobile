package com.pluribus.tools.files

import com.pluribus.PluribusApplication
import java.io.File
import java.io.IOException

object FileHelper {

    @Throws(IOException::class)
    fun generateFile(fileName: String): File? {
        val file = File(PluribusApplication.application.filesDir, fileName)
        if (!file.exists()) {
            file.createNewFile()
        }
        return file
    }
}