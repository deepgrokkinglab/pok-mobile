package com.pluribus.tools

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.pluribus.R
import org.jetbrains.anko.toast

fun Context.isAppInstalled(packageName: String?): Boolean {
    if (packageName.isNullOrBlank()) return false
    return try {
        this.packageManager.getPackageInfo(packageName, 0)
        true
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }
}

fun AppCompatActivity.configureActionBarForPluribusPoker(title: String? = null) {
    val supportActionBar = this.supportActionBar
    if (supportActionBar != null) {
        supportActionBar.setHomeButtonEnabled(true)
        supportActionBar.setDisplayHomeAsUpEnabled(true)
        supportActionBar.setHomeAsUpIndicator(R.drawable.ic_back_arrow)
        title?.let {
            supportActionBar.setDisplayShowTitleEnabled(true)
            supportActionBar.title = it
        } ?: supportActionBar.setDisplayShowTitleEnabled(false)
    }
}

fun Context.longToastRes(@StringRes strResId: Int): Toast = Toast
    .makeText(this, strResId, Toast.LENGTH_LONG)
    .apply { show() }

fun Context.openUrl(url: String, toastIfNotOpened: Boolean = true): Boolean {
    val parsedUri = Uri.parse(url)
    val intent = Intent(Intent.ACTION_VIEW, parsedUri)
    return if (intent.resolveActivity(this.packageManager) != null) {
        startActivity(intent)
        true
    } else {
        if (toastIfNotOpened) {
            (getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager)
                ?.setPrimaryClip(ClipData.newPlainText("text", url))
            toast(this.resources.getString(R.string.generic_didCopy_arg, url))
        }
        false
    }
}

fun Activity.hideKeyboard() {
    var a = this
    a.currentFocus?.let { v ->
        val imm = a.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(v.windowToken, 0)
    }
}
