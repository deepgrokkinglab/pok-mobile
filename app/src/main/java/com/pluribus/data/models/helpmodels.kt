package com.pluribus.data.models

data class HelpViewData(val action: String, val amount: Int, val info: String)
data class HelpViewModel(val token: String, val type_answer: String, val data: HelpViewData?)
data class HelpResponse(val response: HelpViewModel, val uuid: String?)
