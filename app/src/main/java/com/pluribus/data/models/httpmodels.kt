package com.pluribus.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.pluribus.BuildConfig
import okhttp3.MultipartBody

data class UserDataRequest(
    val uoken: String,
    val useProxy: Boolean = false,
    val appId: Int = BuildConfig.APP_ID,
    val vc: Int = BuildConfig.VERSION_CODE,
    val brand: String = BuildConfig.BRAND,
)

data class UserDataResult(val woken: String, val uid: Long, val username: String, val proxy: ProxyModel?)

data class LoginRequest(
    val username: String,
    val password: String,
    val appId: Int = BuildConfig.APP_ID,
    val vc: Int = BuildConfig.VERSION_CODE,
    val brand: String = BuildConfig.BRAND,
)
data class ServerResult(val servers: List<Server>)
data class Server(val ip: String, val description: String)

data class LoginResult(val token: String)

data class RegistrationResult(val regId: Int, val token: String?)

data class SendSmsRequest(val regId: Int)

data class ConfirmSmsRequest(val regId: Int, val code: String)

data class UploadLogsRequest(val uoken: String, val file: MultipartBody.Part)

data class RegConfirmSmsResult(
    val correct: Boolean,
    val expired: Boolean,
    val uoken: String?
)
data class ResetPasswordRequest(val email: String)

data class ConfirmResetPasswordRequest(val password_reset_code: String, val email: String, val password: String )

data class ProxyModel(
    val host: String,
    val port: String,
    val auth: Auth
) {
    data class Auth(val username: String, val password: String)
}

class RegistrationRequest {

    @SerializedName("email")
    @Expose
    private var email: String = ""

    @SerializedName("phone")
    @Expose
    private var phone: String = ""

    @SerializedName("password")
    @Expose
    private var password: String = ""

    @SerializedName("username")
    @Expose
    private var username: String = ""

    @SerializedName("isEmail")
    @Expose
    private var isEmail: Boolean = false

    @SerializedName("vc")
    @Expose
    val vc: Int = BuildConfig.VERSION_CODE

    @SerializedName("vc")
    @Expose
    val brand: String = BuildConfig.BRAND

    fun setEmail(email: String?) {
        this.email = email!!
    }

    fun setPhone(phone: String?) {
        this.phone = phone!!
    }

    fun setPassword(password: String?) {
        this.password = password!!
    }

    fun setUsername(username: String?) {
        this.username = username!!
    }

    fun setIsEmail(isEmail: Boolean) {
        this.isEmail = isEmail!!
    }


}

class VerificationCodeExpiredException : Exception()