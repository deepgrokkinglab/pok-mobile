package com.pluribus.data.models

import android.graphics.Typeface
import android.util.TypedValue
import android.widget.TextView

data class TextStyle(val textColor: Int, val textSize: Float, val typeface: Int)

fun TextStyle.applyTo(tv: TextView) {
    val a = this
    tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, a.textSize)
    tv.setTypeface(tv.typeface, if (a.typeface == Typeface.NORMAL) (a.typeface + 100) else a.typeface)
    tv.setTextColor(a.textColor)
}
