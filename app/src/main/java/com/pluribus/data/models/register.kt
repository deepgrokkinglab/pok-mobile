package com.pluribus.data.models

import java.io.Serializable

enum class LoginType { PHONE, EMAIL, MIXED }

data class RegistrationData(
    val loginType: LoginType,
    val login: String,
    val registrationId: Int
) : Serializable
