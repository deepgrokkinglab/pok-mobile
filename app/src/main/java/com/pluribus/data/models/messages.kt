package com.pluribus.data.models

data class CouponViewData(val success: Boolean, val code: String?)
data class CouponViewModel(val serviceAction: String, val data: CouponViewData)

data class AppVerData(val url: String, val version: String)
data class AppVerModel(val serviceAction: String, val data: AppVerData)

data class CouponSubscriptionData(val paidTill: Long, val activatedAt: Long, val maxLimitEnc: Long)
data class CouponSubscriptionModel(val serviceAction: String, val data: CouponSubscriptionData)

data class ContactsData(val type: Int, val url: String, val name: String, val scopes: List<String>, val langs: List<String>)
data class ContactsModel(val serviceAction: String, val data: List<ContactsData>)
