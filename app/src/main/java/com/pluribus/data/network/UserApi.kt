package com.pluribus.data.network

import com.pluribus.data.models.ContactsModel
import io.reactivex.Single
import retrofit2.http.GET

interface UserApi {

    @GET("get_contacts")
    fun getContacts(): Single<ContactsModel>
}