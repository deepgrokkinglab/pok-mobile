package com.pluribus.data.network

import com.pluribus.data.models.*
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    @POST("token/generatewoken2")
    fun userData(@Body request: UserDataRequest): Single<UserDataResult>

    @POST("/token/generateuoken")
    fun login(@Body request: LoginRequest): Single<LoginResult>

    @POST("users")
    fun startRegistration(@Body request: RegistrationRequest): Single<RegistrationResult>

    @POST("reg/sendsms")
    fun regSendSms(@Body request: SendSmsRequest): Completable

    @POST("reg/confirmsms")
    fun regConfirmSms(@Body request: ConfirmSmsRequest): Single<RegConfirmSmsResult>

    @POST("initiate_reset_password")
    fun resetPassword(@Body request: ResetPasswordRequest): Completable

    @POST("confirm_reset_password")
    fun confirmResetPassword(@Body request: ConfirmResetPasswordRequest): Completable
}