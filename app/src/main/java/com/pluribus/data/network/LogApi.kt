package com.pluribus.data.network;

import io.reactivex.Completable;
import okhttp3.MultipartBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

interface LogApi {

    @Multipart
    @POST("upload/logs")
    fun uploadLogs(@Query("auth") uoken: String, @Part file: MultipartBody.Part): Completable

}
