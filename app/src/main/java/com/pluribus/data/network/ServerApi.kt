package com.pluribus.data.network

import com.pluribus.data.models.ServerResult
import io.reactivex.Single
import retrofit2.http.GET

interface ServerApi {

    @GET("servers")
    fun getServers(): Single<ServerResult>
}