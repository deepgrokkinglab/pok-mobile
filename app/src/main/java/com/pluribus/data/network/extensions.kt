package com.pluribus.data.network

import java.io.IOException

fun Throwable.isNetworkError(): Boolean {
    return this is IOException
}