package com.pluribus.data.repository.ws

import android.util.Base64
import com.google.gson.Gson
import com.pluribus.BuildConfig
import com.pluribus.PluribusApplication
import com.pluribus.data.models.*
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.domain.repositories.ServerRepository
import com.pluribus.domain.repositories.SettingsRepository
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.kodein
import com.pluribus.presentation.record.LabelsService
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.generic.instance
import timber.log.Timber
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URLEncoder
import java.security.SecureRandom
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager

private val QUEUE_SIZE_LIMIT: Long = 1 * 256 * 512

class WSRepositoryImpl(
        private val sslHost: String,
        private val unsafeHost: String,
        private val authRepository: AuthRepository,
        settingsRepository: SettingsRepository
) : WSRepository {

    private val wokenSubject = BehaviorSubject.create<Pair<String, ProxyModel?>>()
    private val closeSocket = BehaviorSubject.create<Boolean>() //true if need reconnect
    private val serverRepository by kodein.instance<ServerRepository>()
    private var _started = false
    override val started: Boolean
        get() = _started

    override val observeConnection = BehaviorSubject.create<Boolean>()
    override val observeHelp = BehaviorSubject.create<HelpViewModel>()
    override val observeCoupon = PublishSubject.create<CouponViewModel>()
    override val observeSubscription = BehaviorSubject.create<CouponSubscriptionModel>()
    override val observeContacts = BehaviorSubject.create<ContactsModel>()
    override val observeLogout = PublishSubject.create<Boolean>()
    override val observeUpdate = BehaviorSubject.create<AppVerModel>()
    override val observeHost = BehaviorSubject.create<Server>() // Contains LATEST host we've tried to connect to

    private var ws: WebSocket? = null
    private var sentNumber = 0L
    private var useProxy: Boolean

    private var pendingRequests: MutableList<String> = mutableListOf()

    private val statsMap: MutableMap<String, Long> = mutableMapOf()

    init {
        useProxy = settingsRepository.getUseProxy()

        closeSocket
            .observeOn(Schedulers.io())
            .flatMapCompletable { reconnect ->
                if (reconnect && started) {
                    authRepository
                        .getUserToken()
                        ?.let {
                            authRepository
                                .userData(it, useProxy)
                                .doOnSuccess { result ->
                                    wokenSubject.onNext(result.woken to result.proxy)
                                    sendPendingRequests()
                                }
                                .doOnError {
                                    closeSocket.onNext(true)
                                }
                                .ignoreElement()
                                .onErrorComplete()
                        }
                        ?: Completable.complete()
                } else {
                    Completable.complete()
                }
            }
            .subscribe()

        wokenSubject
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    closeConnection()
                    sentNumber = 0

                }.zipWith(serverRepository.getFastestServer().toObservable()) { pair: Pair<String, ProxyModel?>, server: Server ->
                    if (server.ip.isNotBlank()) {
                        connect(pair.first, pair.second, server.ip)
                    } else connect(pair.first, pair.second)
                }
                .subscribe()

        settingsRepository.getUseProxyObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    if (useProxy != it && started) {
                        useProxy = it
                        closeSocket.onNext(true)
                    }
                }
                .subscribe()
    }

    override fun sendCoupon(code: String, roomId: Int, brand: String) {
        Timber.d("activate coupon $roomId")
        ws?.send("""{ "serviceAction":"couponActivate", "data": { "code":"$code", "roomId":"$roomId", "brand":"$brand" } }""".trimIndent())
    }

    override fun sendStatistics(uuid: String) {
        val previousValue = statsMap[uuid]
        val currentTimestamp = System.currentTimeMillis()

        val latency = if (previousValue != null) currentTimestamp - previousValue else 0
        val request = """{ "serviceAction":"sendStatistics", "data": { "uuid":"$uuid", "timestamp": "$currentTimestamp", "latency":"$latency" } }""".trimIndent()
        ws?.send(request)
        statsMap.remove(uuid)
    }

    override fun sendPendingRequests() {
        pendingRequests.forEach { request ->
            ws?.send(request)
        }
        pendingRequests.clear()
    }

    override fun sendScreenShot(screenShot: ByteArray, isBug: Boolean) {
        val mss = Base64
                .encodeToString(screenShot, Base64.DEFAULT)
                .replace("\n", "")

        val uuid = UUID.randomUUID().toString()
        val timestamp = System.currentTimeMillis()
        val message = """{ "serviceAction":"recognize", "data": {"uuid":"$uuid", "isBug": $isBug, "token":"test", "timestamp":"$timestamp", "counter":"${sentNumber++}", "image":"$mss" } }"""
        statsMap[uuid] = timestamp

        if ((ws?.queueSize() ?: 0) < QUEUE_SIZE_LIMIT) {
            ws?.send(message.trimIndent())
        }
    }

    override fun requestSubscriptionState(roomId: Int, brand: String) {
        ws?.send("""{ "serviceAction":"checkSubscription", "data": { "roomId":"$roomId", "brand":"$brand" } }""".trimIndent())
    }

    override fun requestContacts() {
        ws?.send("""{ "serviceAction":"getContacts", "data": { } }""".trimIndent())
    }

    override fun requestContacts(agentId: String, roomId: Int) {
        Timber.d("request cobtacts, $agentId $roomId")
        ws?.send("""{ "serviceAction":"getContacts", "data": { "agentId":"$agentId", "roomId":"$roomId" } }""".trimIndent())
//        ws?.send("""{ "serviceAction":"getContacts", "data": {  } }""".trimIndent())
    }

    override fun start(wsToken: String) {
        _started = true
        wokenSubject.onNext(wsToken to null)
    }

    override fun stop() {
        statsMap.clear()
        _started = false
        val w = ws
        ws = null
        w?.close(1000, "by stop")
    }

    private fun closeConnection() {
        statsMap.clear()
        val w = ws
        ws = null
        w?.close(1000, "by closeConnection")
    }

    override fun connect(wsToken: String, proxyModel: ProxyModel?, customHost: String?) {
        _started = true
        val proxyConnect = useProxy && proxyModel != null

        val tm = EmptyTrustManager()
        val trustAllCerts: Array<TrustManager> = arrayOf<TrustManager>(tm)

        val sslContext: SSLContext = SSLContext.getInstance("SSL")
                .apply { init(null, trustAllCerts, SecureRandom()) }

        val client = OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .pingInterval(5, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .sslSocketFactory(sslContext.socketFactory, tm)
                .hostnameVerifier { _, _ -> true }
                .apply {
                    if (proxyConnect && proxyModel != null) {
                        val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(proxyModel.host, proxyModel.port.toInt()))
                        val proxyAuthenticator = Authenticator { route, response ->
                            val credential = Credentials.basic(proxyModel.auth.username, proxyModel.auth.password)
                            response.request.newBuilder()
                                    .header("Proxy-Authorization", credential)
                                    .build()
                        }
                        proxy(proxy)
                        proxyAuthenticator(proxyAuthenticator)
                    }
                }
                .build()

        val queryString =
                linkedMapOf(
                        "appId" to BuildConfig.APP_ID,
                        "woken" to wsToken,
                        "vc" to BuildConfig.VERSION_CODE,
                        "vn" to BuildConfig.VERSION_NAME,
                        "agentId" to BuildConfig.AGENT_ID,
                        "roomId" to BuildConfig.ROOM_ID,
                ).map { e -> "${e.key}=${URLEncoder.encode("" + e.value, Charsets.UTF_8.name())}" }
                        .joinToString("&")

        Timber.d("connect $queryString")
        val host = if (proxyConnect && customHost != null) {
            "wss://$customHost:8000/receive"
        } else if (proxyConnect) {
            sslHost
        } else if (customHost != null) {
            "ws://$customHost:8000/receive"
        } else {
            unsafeHost
        }
        observeHost.onNext(Server(host, "WsRepository host"))

        val url = "${host}?${queryString}"

        val request = Request.Builder()
                .get()
                .url(url)
                .build()

        ws = client.newWebSocket(request, object : WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                observeConnection.onNext(true)
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                handleMessage(text)
            }

            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                observeConnection.onNext(false)
            }

            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                observeConnection.onNext(false)
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                pendingRequests.add("""{ "serviceAction":"sendStatistics", "data": { "timestamp": "${System.currentTimeMillis()}", "dropConnection":"true" } }""".trimIndent())
                observeConnection.onNext(false)
                closeSocket.onNext(true)
            }
        })
    }

    private fun handleMessage(text: String) {
        try {
            Timber.d("handle $text")
            when {
                text.contains("type_answer") -> {
                    var newText = text
                    if (newText.contains("\"null\"")) {
                        newText = newText.replace("\"null\"", "null")
                    }
                    val helpResponse = Gson().fromJson(newText, HelpResponse::class.java)
                    observeHelp.onNext(helpResponse.response)
                    helpResponse.uuid?.let { uuid -> sendStatistics(uuid) }
                }
                text.contains("couponActivate") -> {
                    observeCoupon.onNext(Gson().fromJson(text, CouponViewModel::class.java))
                }
                text.contains("checkSubscription") -> {
                    observeSubscription.onNext(Gson().fromJson(text, CouponSubscriptionModel::class.java))
                }
                text.contains("getContacts") -> {
                    observeContacts.onNext(Gson().fromJson(text, ContactsModel::class.java))
                }
                text.contains("updateApk") -> {
                    observeUpdate.onNext(Gson().fromJson(text, AppVerModel::class.java))
                }
                text.contains("logout") -> {
                    LabelsService.stopService(PluribusApplication.application)
                    authRepository.logout()
                    observeLogout.onNext(true)
                    stop()
                }
            }
        } catch (ignored: Throwable) {
        }
    }
}