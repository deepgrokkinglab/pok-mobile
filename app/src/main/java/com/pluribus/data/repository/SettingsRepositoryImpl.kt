package com.pluribus.data.repository

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import com.pluribus.data.GSON
import com.pluribus.data.models.TextStyle
import com.pluribus.domain.repositories.SettingsRepository
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

private const val PREFS_NAME = "SETTINGS_PREFS"

private const val USER_NAME_KEY = "user_name"
private const val LANGUAGE_KEY = "lang"
private const val TEXT_STYLE_KEY = "text_style"
private const val USE_PROXY = "use_proxy"
private const val CURRENT_PROXY = "current_proxy"

class SettingsRepositoryImpl(context: Context) : SettingsRepository {

    private val prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    private val prefsChangeSubject = BehaviorSubject.createDefault("")
    private val prefsChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sp, key ->
        prefsChangeSubject.onNext(key)
    }

    init {
        prefs.registerOnSharedPreferenceChangeListener(prefsChangeListener)
    }

    override fun saveUserName(userName: String) {
        prefs.edit().putString(USER_NAME_KEY, userName).apply()
    }

    override fun getUserName(): String {
        return prefs.getString(USER_NAME_KEY, "")!!
    }

    override fun saveLanguage(lang: String) {
        prefs.edit().putString(LANGUAGE_KEY, lang).apply()
    }

    override fun getLanguage(): String {
        return prefs.getString(LANGUAGE_KEY, "")!!
    }

    override fun saveTextStyle(textStyle: TextStyle) {
        prefs.edit().putString(TEXT_STYLE_KEY, GSON.toJson(textStyle)).apply()
    }

    override fun getTextStyle(): TextStyle {
        return prefs.getString(TEXT_STYLE_KEY, null)
            ?.let { GSON.fromJson(it, TextStyle::class.java) }
            ?: TextStyle(0xFF0AAC0A.toInt(), 20f, Typeface.BOLD)
    }

    override fun getOptionalTextStyle(): TextStyle? {
        return prefs.getString(TEXT_STYLE_KEY, null)
                ?.let { GSON.fromJson(it, TextStyle::class.java) }
    }

    override fun setUseProxy(useProxy: Boolean) {
        prefs.edit().putBoolean(USE_PROXY, useProxy).apply()
    }

    override fun getUseProxy() = prefs.getBoolean(USE_PROXY, false)

    override fun getUseProxyObservable(): Observable<Boolean> {
        return prefsChangeSubject
                .startWith(USE_PROXY)
                .filter { it == USE_PROXY }
                .map { prefs.getBoolean(USE_PROXY, false) }
    }
}