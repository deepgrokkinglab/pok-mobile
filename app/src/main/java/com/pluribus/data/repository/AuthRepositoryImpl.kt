package com.pluribus.data.repository

import android.content.Context
import com.pluribus.BuildConfig
import com.pluribus.data.models.*
import com.pluribus.data.network.AuthApi
import com.pluribus.domain.repositories.AuthRepository
import io.reactivex.Completable
import io.reactivex.Single

private const val AUTH_PREFS_NAME = "auth_prefs"
private const val USER_TOKEN_KEY = "uoken"

class AuthRepositoryImpl(
    context: Context,
    private val api: AuthApi
) : AuthRepository {

    private val prefs = context.getSharedPreferences(AUTH_PREFS_NAME, Context.MODE_PRIVATE)

    override fun startRegistration(request: RegistrationRequest): Single<RegistrationResult> {
        return api.startRegistration(request).doOnSuccess {
            prefs.edit().putString(USER_TOKEN_KEY, it.token).apply()
        }
    }

    override fun sendSms(request: SendSmsRequest): Completable {
        return api.regSendSms(request)
    }

    override fun confirmSms(request: ConfirmSmsRequest): Completable {
        return api
            .regConfirmSms(request)
            .flatMapCompletable { result ->
                when {
                    result.correct -> {
                        Completable
                            .fromCallable {
                                prefs.edit().putString(USER_TOKEN_KEY, result.uoken).apply()
                            }
                    }
                    result.expired -> {
                        Completable.error(VerificationCodeExpiredException())
                    }
                    else -> {
                        Completable.error(RuntimeException())
                    }
                }
            }
    }

    override fun login(login: String, password: String): Completable {
        return api
            .login(LoginRequest(login, password, BuildConfig.APP_ID))
            .flatMapCompletable {
                Completable.fromAction { prefs.edit().putString(USER_TOKEN_KEY, it.token).apply() }
            }
    }

    override fun userData(token: String, useProxy: Boolean): Single<UserDataResult> {
        return api.userData(UserDataRequest(token, useProxy))
    }

    override fun getUserToken(): String? {
        return prefs.getString(USER_TOKEN_KEY, null)
    }

    override fun logout() {
        prefs.edit().clear().apply()
    }

    override fun resetUserPassword(request: ResetPasswordRequest): Completable {
        return api.resetPassword(request)
    }

    override fun confirmResetUserPassword(request: ConfirmResetPasswordRequest): Completable {
        return api.confirmResetPassword(request)
    }
}