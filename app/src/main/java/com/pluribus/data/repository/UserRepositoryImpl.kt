package com.pluribus.data.repository

import com.pluribus.data.models.ContactsModel
import com.pluribus.data.network.UserApi
import com.pluribus.domain.repositories.UserRepository
import io.reactivex.Single

class UserRepositoryImpl(private val api: UserApi) : UserRepository {
    override fun getContacts(): Single<ContactsModel> {
        return api.getContacts()
    }

}