package com.pluribus.data.repository

import com.pluribus.data.models.UploadLogsRequest
import com.pluribus.data.network.LogApi
import com.pluribus.domain.repositories.LogRepository
import io.reactivex.Completable

class LogRepositoryImpl(
        private val api: LogApi
) : LogRepository {

    override fun uploadLogs(uploadLogsRequest: UploadLogsRequest): Completable {
        return api.uploadLogs(uoken = uploadLogsRequest.uoken, uploadLogsRequest.file)
    }

}