package com.pluribus.data.repository

import com.pluribus.data.models.Server
import com.pluribus.data.models.ServerResult
import com.pluribus.data.network.ServerApi
import com.pluribus.domain.repositories.ServerRepository
import com.pluribus.tools.countPingToResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.URL

class ServerRepositoryImpl(private val serverApi: ServerApi) : ServerRepository {

    override fun getServers(): Single<ServerResult> {
        return serverApi.getServers()
    }

    override fun getFastestServer(): Single<Server> {
        return serverApi.getServers()
                .flatMap {
                    Single.just(
                            it.servers.minByOrNull { server ->
                                URL("http://${server.ip}").countPingToResource()
                            }!!)
                }.onErrorReturn { Server("","") }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}