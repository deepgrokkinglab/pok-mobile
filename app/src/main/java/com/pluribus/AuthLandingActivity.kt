package com.pluribus

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.pluribus.presentation.auth.AuthActivity
import com.pluribus.presentation.auth.RegistrationActivity
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.main.support.GuestSupportActivity
import kotlinx.android.synthetic.main.activity_auth_landing.*

class AuthLandingActivity : BaseActivity() {
    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, AuthLandingActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth_landing)

        startLogInButton.setOnClickListener { AuthActivity.startActivity(this) }
        startRegisterButton.setOnClickListener { startActivity(Intent(this, RegistrationActivity::class.java)) }
        supportTextView.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }
    }
}