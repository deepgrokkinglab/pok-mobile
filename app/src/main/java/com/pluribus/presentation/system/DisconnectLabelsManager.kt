package com.pluribus.presentation.system

import android.app.Service
import android.content.Context
import android.graphics.PixelFormat
import android.os.Build
import android.view.*
import com.pluribus.R

class DisconnectLabelsManager(private val context: Context) {

    private val windowManager = context.getSystemService(Service.WINDOW_SERVICE) as WindowManager

    private var labelView: View? = null
    private var lastState: Boolean? = null

    fun showDisconnect(disconnect: Boolean) {
        if (lastState == disconnect) return

        lastState = disconnect

        labelView?.let {
            windowManager.removeView(it)
            labelView = null
        }

        if (!disconnect) return

        val layoutParams = WindowManager.LayoutParams().apply {
            if (Build.VERSION.SDK_INT >= 26) {
                type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                @Suppress("DEPRECATION")
                type = WindowManager.LayoutParams.TYPE_PHONE
            }
            flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
            format = PixelFormat.RGBA_8888
            gravity = 0.or(Gravity.START).or(Gravity.BOTTOM)
            width = ViewGroup.LayoutParams.WRAP_CONTENT
            height = ViewGroup.LayoutParams.WRAP_CONTENT
            gravity = Gravity.TOP or Gravity.END
        }

        labelView = LayoutInflater
            .from(context)
            .inflate(R.layout.overlay_status, null)

        windowManager.addView(labelView, layoutParams)

    }

}