package com.pluribus.presentation.system

import android.content.Context
import android.content.Context.MEDIA_PROJECTION_SERVICE
import android.content.Context.WINDOW_SERVICE
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PixelFormat
import android.graphics.Point
import android.hardware.display.VirtualDisplay
import android.media.ImageReader
import android.media.projection.MediaProjection
import android.media.projection.MediaProjectionManager
import android.os.Handler
import android.os.HandlerThread
import android.os.Process
import android.view.Display
import android.view.WindowManager
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.io.ByteArrayOutputStream

private val VIRT_DISPLAY_FLAGS: Int =
    android.hardware.display.DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY or
        android.hardware.display.DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC


class ScreenShooter(private val context: Context) : ImageReader.OnImageAvailableListener {

    var waitingNewImage = false

    private var width = 0
    private var height = 0

    private val handlerThread = HandlerThread(
        javaClass.simpleName,
        Process.THREAD_PRIORITY_BACKGROUND
    ).apply { start() }
    private val handler = Handler(handlerThread.looper)
    private val imageReader: ImageReader

    init {
        val windowManager = (context.getSystemService(WINDOW_SERVICE) as WindowManager)
        val display: Display = windowManager.defaultDisplay
        val size = Point()

        display.getRealSize(size)

        var width = size.x
        var height = size.y

        while (width * height > 2 shl 19) {
            width = (width * 0.9).toInt()
            height = (height * 0.9).toInt()
        }

        this.width = width
        this.height = height

        imageReader = if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1){
            ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, 2)
        } else {
            ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, 1)
        }

        imageReader.setOnImageAvailableListener(this, handler)
    }

    private val mediaProjectionManager = context.getSystemService(MEDIA_PROJECTION_SERVICE) as MediaProjectionManager

    val observe: Observable<ByteArray> = BehaviorSubject.create()

    private var virtualDisplay: VirtualDisplay? = null
    private var projection: MediaProjection? = null

    private var prepareBusy = false

    private val projectionCallback = object : MediaProjection.Callback() {
        override fun onStop() {
            virtualDisplay?.release()
        }
    }

    fun startCapture(
        resultCode: Int,
        resultData: Intent
    ) {
        projection = mediaProjectionManager.getMediaProjection(resultCode, resultData)
        virtualDisplay = projection!!.createVirtualDisplay(
            "andshooter",
            width,
            height,
            context.resources.displayMetrics.densityDpi,
            VIRT_DISPLAY_FLAGS,
            imageReader.surface,
            null,
            handler
        )
        projection?.registerCallback(projectionCallback, handler)
    }

    fun stopCapture() {
        projection?.unregisterCallback(projectionCallback)
        projection?.stop()
        virtualDisplay?.release()
        imageReader.close()
    }

    override fun onImageAvailable(reader: ImageReader?) {
        try {
            val image = imageReader.acquireLatestImage()
            if (!waitingNewImage || prepareBusy) {
                image?.close()
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                    imageReader.discardFreeBuffers()
                }
            } else if (image != null) {
                prepareBusy = true
                val planes = image.planes
                val buffer = planes[0].buffer
                buffer.rewind()
                val pixelStride = planes[0].pixelStride
                val rowStride = planes[0].rowStride
                val rowPadding = rowStride - pixelStride * width
                val bitmapWidth = width + rowPadding / pixelStride
                val latestBitmap = Bitmap.createBitmap(bitmapWidth, height, Bitmap.Config.ARGB_8888)
                latestBitmap!!.copyPixelsFromBuffer(buffer)
                val baos = ByteArrayOutputStream()

                val croppedSupersampled = Bitmap
                        .createBitmap(latestBitmap, 0, 0, width, height)

                croppedSupersampled!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)

                (observe as BehaviorSubject).onNext(baos.toByteArray())

                croppedSupersampled.recycle()
                baos.close()
                latestBitmap.recycle()
                image.close()
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                    imageReader.discardFreeBuffers()
                }
                prepareBusy = false
                waitingNewImage = false
            }
        } catch (e: Exception) {
            try {
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                    imageReader.discardFreeBuffers()
                }
            } catch (e2: Exception) {
                e2.printStackTrace()
            }
            prepareBusy = false
            waitingNewImage = false
            e.printStackTrace()
        }
    }
}