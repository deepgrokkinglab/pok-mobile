package com.pluribus.presentation.system

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.provider.Settings
import android.view.Surface
import android.view.WindowManager
import androidx.core.content.ContextCompat.getSystemService
import com.pluribus.tools.*


class DeviceOrientationManager(val context: Context) {

    fun isDeviceDefaultOrientationLandscape(): Boolean {

        val windowManager = getSystemService(context, WindowManager::class.java) as WindowManager
        val config: Configuration = context.resources.configuration
        val rotation = windowManager.defaultDisplay.rotation

        val defaultLandscapeAndIsInLandscape = (rotation == Surface.ROTATION_0 ||
                rotation == Surface.ROTATION_180) &&
                config.orientation == Configuration.ORIENTATION_LANDSCAPE

        val defaultLandscapeAndIsInPortrait = (rotation == Surface.ROTATION_90 ||
                rotation == Surface.ROTATION_270) &&
                config.orientation == Configuration.ORIENTATION_PORTRAIT

        return defaultLandscapeAndIsInLandscape || defaultLandscapeAndIsInPortrait
    }

    fun shouldDisableSystemAutoRotation(isEnabled: Boolean) {
        Settings.System.putInt(
                context.contentResolver,
                Settings.System.ACCELEROMETER_ROTATION,
                isEnabled.toInt()
        )
    }

    // Use with Surface.ROTATION_0 || Surface.ROTATION_90 || Surface.ROTATION_180 || Surface.ROTATION_270
    fun lockScreenOrientationTo(orientation: Int) {
        Settings.System.putInt(
                context.contentResolver,
                Settings.System.USER_ROTATION,
                orientation
        )
    }

    fun checkShouldRequestSettings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.System.canWrite(context)) {
            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}