package com.pluribus.presentation.system

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.icu.util.TimeUnit
import android.os.Build
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import com.pluribus.BuildConfig
import com.pluribus.R
import com.pluribus.data.GSON
import com.pluribus.data.models.HelpViewData
import com.pluribus.data.models.HelpViewModel
import com.pluribus.domain.repositories.SettingsRepository
import com.pluribus.presentation.kodein
import com.pluribus.tools.goneIf
import com.pluribus.tools.invisibleIf
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.overlay_decision.view.*
import org.jetbrains.anko.displayMetrics
import org.kodein.di.generic.instance

@SuppressLint("ResourceType")
class LabelsManager(
        private val context: Context,
        private val isBugClickListener: () -> Unit
) {

    private val windowManager = context.getSystemService(Service.WINDOW_SERVICE) as WindowManager

    private val decisionResumeBg: Int
    private val decisionFoldBg: Int
    private val decisionWaitingBg: Int
    private val decisionResumeTextColor: Int
    private val decisionFoldTextColor: Int
    private val decisionWaitingTextColor: Int
    private val showConnectionStatus: Boolean
    private val settingsRepository by kodein.instance<SettingsRepository>()
    private var timerDisposable: Disposable? = null

    init {
        context.theme.applyStyle(R.style.AppThemeBase, true)
        val typedArray = context.theme.obtainStyledAttributes(intArrayOf(
                R.attr.myOverlayDecisionResumeBg,
                R.attr.myOverlayDecisionFoldBg,
                R.attr.myOverlayDecisionWaitingBg,
                R.attr.myOverlayResumeTextColor,
                R.attr.myOverlayFoldTextColor,
                R.attr.myOverlayWaitingTextColor,
                R.attr.myOverlayConnectionStatusVisible
        ))

        val textStyle = settingsRepository.getOptionalTextStyle()
        if (textStyle != null) {
            decisionResumeBg = typedArray.getResourceId(0, 0)
            decisionFoldBg = typedArray.getResourceId(1, 0)
            decisionWaitingBg = typedArray.getResourceId(2, 0)
            decisionResumeTextColor = textStyle.textColor
            decisionFoldTextColor = textStyle.textColor
            decisionWaitingTextColor = textStyle.textColor
            showConnectionStatus = typedArray.getBoolean(6, false)
        } else {
            decisionResumeBg = typedArray.getResourceId(0, 0)
            decisionFoldBg = typedArray.getResourceId(1, 0)
            decisionWaitingBg = typedArray.getResourceId(2, 0)
            decisionResumeTextColor = typedArray.getColor(3, Color.WHITE)
            decisionFoldTextColor = typedArray.getColor(4, Color.WHITE)
            decisionWaitingTextColor = typedArray.getColor(5, Color.WHITE)
            showConnectionStatus = typedArray.getBoolean(6, false)
        }


        typedArray.recycle()
    }

    private var labelView: View? = null
    private var lastModel: HelpViewModel? = null

    fun showLabel(helpViewModel: HelpViewModel?) {
        if (lastModel == helpViewModel) return

        lastModel = helpViewModel
        showHelpResponse(helpViewModel)
    }

    private fun showHelpResponse(response: HelpViewModel?) {
        if (response == null) {
            labelView?.let {
                windowManager.removeView(it)
                labelView = null
            }
            return
        }

        val oldLabelView = labelView
        val layoutParams: WindowManager.LayoutParams
        val typeAnswer = response.type_answer

        when {
            typeAnswer == "connected" -> {
                layoutParams = WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                        } else {
                            WindowManager.LayoutParams.TYPE_PHONE
                        },
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                )

                layoutParams.gravity = Gravity.TOP or Gravity.CENTER
                labelView = LayoutInflater
                        .from(context)
                        .inflate(R.layout.overlay_connected, null)
                labelView?.invisibleIf(!showConnectionStatus)
            }
            typeAnswer.contains("waiting_for_a_seat") -> {
                layoutParams = WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                        } else {
                            WindowManager.LayoutParams.TYPE_PHONE
                        },
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                )

                layoutParams.gravity = Gravity.TOP
                labelView = LayoutInflater
                        .from(context)
                        .inflate(R.layout.overlay_connecting, null)
                labelView?.invisibleIf(!showConnectionStatus)
            }
            typeAnswer == "not_support" -> {
                layoutParams = WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                        } else {
                            WindowManager.LayoutParams.TYPE_PHONE
                        },
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                )

                layoutParams.gravity = Gravity.TOP or Gravity.CENTER

                labelView = LayoutInflater
                        .from(context)
                        .inflate(R.layout.overlay_error, null)
            }
            typeAnswer == "hint" || typeAnswer == "wait_action_after_hint" || typeAnswer == "waiting" -> {
                layoutParams = WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                        } else {
                            WindowManager.LayoutParams.TYPE_PHONE
                        },
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                )

                layoutParams.gravity = Gravity.TOP or Gravity.END
                labelView = LayoutInflater
                        .from(context)
                        .inflate(R.layout.overlay_decision, null)
                        .apply {
                            btn_report.goneIf(!BuildConfig.IS_AGREE)
                            debug_info_text.goneIf(!BuildConfig.DEBUG)

                            btn_report.setOnClickListener {
                                isBugClickListener()
                            }
                        }

                labelView?.invisibleIf(!showConnectionStatus && typeAnswer != "hint")
                setHelpContent(response.data)

                response
                        .data
                        ?.let { labelView!!.debug_info_text.text = GSON.toJson(it) }
                        ?: let { labelView!!.debug_info_text.text = "null" }

                setHintDestroyerTimer()
            }
            else -> {
                return
            }
        }

        try {
            oldLabelView?.let { windowManager.removeView(it) }
        } catch (ignored: Throwable) {

        }
        windowManager.addView(labelView, layoutParams)
    }

    private fun setHelpContent(data: HelpViewData?) {
        val textView: TextView = labelView!!.findViewById(R.id.overlay_decision_text)

        when (data?.action) {
            "call" -> {
                if (data.amount > 0) {
                    textView.text = context.getString(R.string.PokerActionType_CALL) + data.amount.toString()
                } else {
                    textView.setText(R.string.PokerActionType_CALL)
                }

                textView.setTextColor(decisionResumeTextColor)
                textView.setBackgroundResource(decisionResumeBg)
            }
            "check" -> {
                textView.setText(R.string.PokerActionType_CHECK)
                textView.setTextColor(decisionResumeTextColor)
                textView.setBackgroundResource(decisionResumeBg)
            }
            "raise" -> {
                if (data.amount > 0) {
                    textView.text = context.getString(R.string.PokerActionType_RAISE) + data.amount.toString()
                } else {
                    textView.setText(R.string.PokerActionType_RAISE)
                }

                textView.setTextColor(decisionResumeTextColor)
                textView.setBackgroundResource(decisionResumeBg)
            }
            "allin" -> {
                textView.setText(R.string.PokerActionType_ALLIN)
                textView.setTextColor(decisionResumeTextColor)
                textView.setBackgroundResource(decisionResumeBg)
            }
            "fold" -> {
                textView.setText(R.string.PokerActionType_FOLD)
                textView.setTextColor(decisionFoldTextColor)
                textView.setBackgroundResource(decisionFoldBg)
            }
            else -> {
                textView.setText(R.string.PokerActionType_WAITING)
                textView.setTextColor(decisionWaitingTextColor)
                textView.setBackgroundResource(decisionWaitingBg)
            }
        }
    }

    private fun setHintDestroyerTimer() {
        timerDisposable?.dispose()
        timerDisposable = Observable.timer(4, java.util.concurrent.TimeUnit.SECONDS).subscribe {
            showHelpResponse(null)
        }
    }
}