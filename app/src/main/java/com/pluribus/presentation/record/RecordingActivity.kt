package com.pluribus.presentation.record

import android.content.Context
import android.content.Intent
import android.media.projection.MediaProjectionManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.pluribus.R

class RecordingActivity : AppCompatActivity() {

    companion object {

        private const val MEDIA_PROJECTION_REQUEST_CODE = 179

        fun startActivity(context: Context) {
            context.startActivity(Intent(context, RecordingActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recording)
        val mediaProjectionIntent = (getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager)
            .createScreenCaptureIntent()
        startActivityForResult(mediaProjectionIntent, MEDIA_PROJECTION_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MEDIA_PROJECTION_REQUEST_CODE) {
            if (data != null && resultCode == RESULT_OK) {
                LabelsService.startService(this, resultCode, data)
                startActivity(packageManager.getLaunchIntentForPackage(getString(R.string.poker_app_package)))
                finish()
            } else {
                Toast.makeText(this, "No permissions available", Toast.LENGTH_SHORT).show()
            }
        }
    }

}