package com.pluribus.presentation.record

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.pluribus.R
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.kodein
import com.pluribus.presentation.system.DisconnectLabelsManager
import com.pluribus.presentation.system.LabelsManager
import com.pluribus.presentation.system.ScreenShooter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.kodein.di.generic.instance
import java.util.concurrent.TimeUnit


private const val CHANNEL_ID = "rtpDisplayStreamChannel"

class LabelsService : Service() {

    companion object {
        const val EXTRA_RESULT_CODE = "resultCode"
        const val EXTRA_RESULT_INTENT = "resultIntent"

        private const val SCREENSHOTS_TIMEOUT_MS = 500L

        fun startService(context: Context, resultCode: Int, resultData: Intent) {
            val intent = Intent(context, LabelsService::class.java).apply {
                putExtra(EXTRA_RESULT_CODE, resultCode)
                putExtra(EXTRA_RESULT_INTENT, resultData)
            }
            context.startService(intent)
        }

        fun stopService(context: Context) {
            context.stopService(Intent(context, LabelsService::class.java))
        }

        fun serviceAlive(context: Context): Boolean {
            val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Int.MAX_VALUE)) {
                if (LabelsService::class.java.name == service.service.className) {
                    return true
                }
            }
            return false
        }
    }

    private val wsRepository by kodein.instance<WSRepository>()

    private var screenShooter: ScreenShooter? = null
    private var labelsManager: LabelsManager? = null
    private var disconnectLabelsManager: DisconnectLabelsManager? = null

    private var labelsDisposable: Disposable? = null
    private var screenShooterDisposable: Disposable? = null
    private var connectionDisposable: Disposable? = null
    private var delayDisposable: Disposable? = null

    private var screenShotIsBug: Boolean = false

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
        startAsForeground()
        screenShooter = ScreenShooter(applicationContext)
        labelsManager = LabelsManager(applicationContext) {
            screenShotIsBug = true
            Toast.makeText(this, getString(R.string.report_has_been_sent), Toast.LENGTH_SHORT).show()
        }
        disconnectLabelsManager = DisconnectLabelsManager(applicationContext)

        labelsDisposable = wsRepository
            .observeHelp
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                labelsManager?.showLabel(it)
            }

        screenShooterDisposable = screenShooter
            ?.observe
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnNext {
                wsRepository.sendScreenShot(it, screenShotIsBug)
                screenShotIsBug = false
            }
            ?.subscribe()

        connectionDisposable = wsRepository
            .observeConnection
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { connected ->
                if (!connected) {
                    labelsManager?.showLabel(null)
                }
                disconnectLabelsManager?.showDisconnect(!connected)
            }

        delayDisposable = Observable
            .interval(SCREENSHOTS_TIMEOUT_MS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                screenShooter?.waitingNewImage = true
            }
            .subscribe()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (flags != START_FLAG_RETRY) {
            intent
                ?.let {
                    val resultCode = it.getIntExtra(EXTRA_RESULT_CODE, 1337)
                    val resultData = it.getParcelableExtra(EXTRA_RESULT_INTENT) as? Intent
                    screenShooter?.startCapture(resultCode, resultData!!)
                }
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        labelsDisposable?.dispose()
        screenShooterDisposable?.dispose()
        connectionDisposable?.dispose()
        delayDisposable?.dispose()
        disconnectLabelsManager?.showDisconnect(false)
        screenShooter?.stopCapture()
        screenShooter = null
        labelsManager?.showLabel(null)
        labelsManager = null
    }

    private fun startAsForeground() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            val notification = NotificationCompat
                .Builder(this, "rtpDisplayStreamChannel")
                .setOngoing(true)
                .setContentTitle("")
                .setContentText("").build()
            startForeground(1, notification)
        } else {
            startForeground(1, Notification())
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH)
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                .createNotificationChannel(channel)
        }
    }
}