package com.pluribus.presentation.auth

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.pluribus.data.models.LoginType

class RegistrationAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun createFragment(position: Int): Fragment {
        return if (position == 0) {
            RegistrationFragment.newInstance(LoginType.EMAIL)
        } else {
            RegistrationFragment.newInstance(LoginType.PHONE)
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}