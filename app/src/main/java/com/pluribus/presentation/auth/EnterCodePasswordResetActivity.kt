package com.pluribus.presentation.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pluribus.R
import com.pluribus.presentation.main.support.GuestSupportActivity
import com.pluribus.tools.hideKeyboard
import kotlinx.android.synthetic.main.activity_enter_code_password_reset.*

class EnterCodePasswordResetActivity : AppCompatActivity() {

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, EnterCodePasswordResetActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_code_password_reset)

        pinCodeConfirmation.setOtpCompletionListener { code ->
            hideKeyboard()
            ConfirmPasswordResetActivity.startActivity(this, code)
        }
        btnBackEnterCodePasswordReset.setOnClickListener { onBackPressed() }

        supportEnterCodePasswordReset.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }
    }
}