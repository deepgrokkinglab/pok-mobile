package com.pluribus.presentation.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.pluribus.R
import com.pluribus.data.models.*
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.support.GuestSupportActivity
import com.pluribus.presentation.start.StartActivity
import com.pluribus.tools.configureActionBarForPluribusPoker
import com.pluribus.tools.hideKeyboard
import com.pluribus.tools.longToastRes
import kotlinx.android.synthetic.main.activity_register_verify.*
import org.jetbrains.anko.toast
import org.kodein.di.generic.instance

class RegistrationFinishActivity : BaseActivity() {

    companion object {
        private const val REG_DATA_KEY = "reg_data"
        fun startActivity(context: Context, regData: RegistrationData) {
            val intent = Intent(context, RegistrationFinishActivity::class.java).apply {
                putExtra(REG_DATA_KEY, regData)
            }
            context.startActivity(intent)
        }
    }

    private val authRepository by kodein.instance<AuthRepository>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_verify)

        val regData = intent.getSerializableExtra(REG_DATA_KEY) as RegistrationData

        configureActionBarForPluribusPoker()
        tv_verify.text =
            resources.getString(
                if (regData.loginType == LoginType.PHONE) {
                    R.string.Register_enterThe6DigitCodFromSms_caption
                } else {
                    R.string.Register_enterThe6DigitCodeFromEmail_caption
                }
            )

        pincode.setOtpCompletionListener {
            hideKeyboard()
            verify(it, regData.registrationId)
        }
        pincode.isCursorVisible = false
        resendButton.setOnClickListener {
            sendCode(regData.registrationId)
        }

        supportTextView.text = getString(R.string.support)
        supportTextView.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }

        sendCode(regData.registrationId)

        btn_back.setOnClickListener { onBackPressed() }
    }

    private fun sendCode(regId: Int) {
        executeCompletable(authRepository.sendSms(SendSmsRequest(regId)))
    }

    private fun enableInput(enabled: Boolean) {
        resendButton.isEnabled = enabled
        pincode.isEnabled = enabled
    }

    private fun verify(code: String, regId: Int) {
        enableInput(false)
        executeCompletable(
            authRepository.confirmSms(ConfirmSmsRequest(regId, code)),
            onComplete = {
                longToastRes(R.string.Register_youHaveRegisteredLetsLogin_textLabel)
                StartActivity.startClear(this)
                finish()
            },
            onError = {
                when (it) {
                    is VerificationCodeExpiredException -> {
                        enableInput(true)
                        longToastRes(R.string.Register_errorVerificationCodeExpiredStartRegistrationAgain_toast)
                    }
                    else -> {
                        toast(R.string.Register_errorVerificationFailedDueToNetworkError_toast)
                        enableInput(true)
                    }
                }
            }
        )
    }
}
