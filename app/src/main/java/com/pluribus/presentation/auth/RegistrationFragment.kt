package com.pluribus.presentation.auth

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import com.pluribus.R
import com.pluribus.data.models.LoginType
import com.pluribus.data.models.RegistrationData
import com.pluribus.data.models.RegistrationRequest
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.presentation.kodein
import com.pluribus.presentation.start.StartActivity
import com.pluribus.tools.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_confirm_password_reset.*
import kotlinx.android.synthetic.main.fragment_registration.*
import org.kodein.di.generic.instance
import retrofit2.HttpException
import timber.log.Timber

class RegistrationFragment : Fragment() {
    companion object {
        private const val ARG_LOGIN_TYPE_ORDINAL = "23freb4g342"

        fun newInstance(loginType: LoginType): RegistrationFragment {
            return RegistrationFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_LOGIN_TYPE_ORDINAL, loginType.ordinal)
                }
            }
        }
    }

    private val authRepo by kodein.instance<AuthRepository>()

    private var disposable: Disposable? = null

    private val loginType: LoginType
        get() = LoginType.values()[requireArguments().getInt(ARG_LOGIN_TYPE_ORDINAL)]


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val (loginIconRes, loginHintRes) = when (loginType) {
            LoginType.PHONE -> {
                R.drawable.ic_phone to R.string.phone_number
            }
            else -> {
                R.drawable.ic_arroba to R.string.Login_loginWithEmail_label
            }
        }
        login_icon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), loginIconRes))
        inputLoginEditText.setHint(loginHintRes)

        inputPasswordEditText.transformationMethod = PasswordTransformationMethod()
        password_visiblity.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.design_ic_visibility))
        password_visiblity.setOnClickListener {
            val (newTransMethod, newVisibilityIcon) = if (inputPasswordEditText.transformationMethod == null) {
                PasswordTransformationMethod() to R.drawable.design_ic_visibility
            } else {
                null to R.drawable.design_ic_visibility_off
            }
            inputPasswordEditText.transformationMethod = newTransMethod
            password_visiblity.setImageDrawable(AppCompatResources.getDrawable(requireContext(), newVisibilityIcon))
        }

        registerButton.setOnClickListener {
            register(loginType)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable?.dispose()
    }

    private fun register(type: LoginType) {
        val login = inputLoginEditText.text.toString()
        val password = inputPasswordEditText.text.toString()

        if (type == LoginType.PHONE && !validatePhone(editText = inputLoginEditText)) {
            return
        }
        if (type == LoginType.EMAIL && !validateEmail(editText = inputLoginEditText)) {
            return
        }
        if (!validatePassword(editText = inputPasswordEditText)) {
            return
        }

        val isEmail: Boolean = type == LoginType.EMAIL

        val registerRequest = RegistrationRequest()

        registerRequest.setPassword(password)
        registerRequest.setIsEmail(isEmail)

        if (isEmail) {
            registerRequest.setEmail(login)
            registerRequest.setUsername(login)
        } else {
            registerRequest.setPhone(login)
        }

        progress_bar.goneIf(false)
        registerButton.goneIf(true)
        disposable = authRepo
                .startRegistration(registerRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            progress_bar.goneIf(true)
                            registerButton.goneIf(false)
                            if (result.token == null) {
                                RegistrationFinishActivity
                                        .startActivity(requireContext(), RegistrationData(type, login, result.regId))
                            } else {
                                StartActivity.startClear(requireContext())
                            }
                        },
                        {
                            when {
                                (it as? HttpException)?.code() == 460 -> {
                                    progress_bar.goneIf(true)
                                    registerButton.goneIf(false)
                                    activity?.longToastRes((R.string.Register_UserAlreadyRegistered))
                                }
                                else -> {
                                    progress_bar.goneIf(true)
                                    registerButton.goneIf(false)
                                    activity?.longToastRes(R.string.Register_errorBeginningOfRegistrationFailedDueToNetworkError_toast)
                                }
                            }
                            Timber.d("sadsdasda $it")
                        }
                )
    }
}