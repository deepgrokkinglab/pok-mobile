package com.pluribus.presentation.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.pluribus.R
import com.pluribus.data.models.ResetPasswordRequest
import com.pluribus.domain.interactors.AuthInteractor
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.support.GuestSupportActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.kodein.di.generic.instance
import retrofit2.HttpException

class ResetPasswordActivity : BaseActivity() {

    private val authInteractor by kodein.instance<AuthInteractor>()

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, ResetPasswordActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        resetPasswordButton.setOnClickListener {
            resetPassword(inputLoginEditTextResetPassword.text.toString())
        }

        supportTextView.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }
    }

    private fun resetPassword(email: String) {
        executeCompletable(
                authInteractor.resetPassword(ResetPasswordRequest(email)).observeOn(AndroidSchedulers.mainThread()),
                onComplete = { EnterCodePasswordResetActivity.startActivity(this) },
                onError = { error ->
                    when {
                        (error as? HttpException)?.code() == 461 ->{
                            inputLoginEditTextResetPassword.error = getString(R.string.Login_ResetPassword_UserNotFound)
                        }
                    }
                    println(error.localizedMessage)
                }
        )
    }
}