package com.pluribus.presentation.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.pluribus.R
import com.pluribus.data.network.isNetworkError
import com.pluribus.domain.interactors.AuthInteractor
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.support.GuestSupportActivity
import com.pluribus.presentation.start.StartActivity
import com.pluribus.tools.goneIf
import com.pluribus.tools.validatePassword
import kotlinx.android.synthetic.main.activity_confirm_password_reset.*
import kotlinx.android.synthetic.main.activity_reset_password.supportTextView
import org.kodein.di.generic.instance
import retrofit2.HttpException

class ConfirmPasswordResetActivity : BaseActivity() {

    companion object {
        private const val PASSWORD_RESET_CODE_KEY = "confirmation_code"

        fun startActivity(context: Context, resetCode: String) {
            val intent = Intent(context, ConfirmPasswordResetActivity::class.java).apply {
                putExtra(PASSWORD_RESET_CODE_KEY, resetCode)
            }
            context.startActivity(intent)
        }
    }

    private val authInteractor by kodein.instance<AuthInteractor>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_password_reset)

        supportTextView.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }

        confirmResetPasswordButton.setOnClickListener {
            confirmPasswordReset()
        }
    }

    private fun confirmPasswordReset() {
        if (!validatePassword(inputPasswordEditTextConfirmPassword)) {
            return
        }
        enableViews(false)

        passwordConfirmationProgress.goneIf(false)
        confirmResetPasswordButton.goneIf(true)
        val code = intent.getStringExtra(PASSWORD_RESET_CODE_KEY) ?: ""

        executeCompletable(
                authInteractor.confirmPasswordReset(code,
                        inputLoginEditTextConfirmPassword.text.toString(),
                        inputPasswordEditTextConfirmPassword.text.toString()

                ),
                onComplete = {
                    StartActivity.startClear(this)
                    passwordConfirmationProgress.goneIf(true)
                },
                onError = {
                    when {
                        (it as? HttpException)?.code() == 500 -> {
                            passwordConfirmationProgress.goneIf(false)
                            confirmResetPasswordButton.goneIf(true)
                            inputPasswordEditTextConfirmPassword.error = getString(R.string.Login_ServerInternalErrorTryAgain)
                            enableViews(true)
                        }
                        (it as? HttpException)?.code() == 461 -> {
                            inputPasswordEditTextConfirmPassword.error = getString(R.string.Login_ResetPassword_UserNotFound)
                            passwordConfirmationProgress.goneIf(false)
                            confirmResetPasswordButton.goneIf(true)
                            enableViews(true)
                        }
                        (it as? HttpException)?.code() == 464 -> {
                            inputPasswordEditTextConfirmPassword.error = getString(R.string.ResetPassword_WrongVerificationCode)
                            passwordConfirmationProgress.goneIf(false)
                            confirmResetPasswordButton.goneIf(true)
                            enableViews(true)
                        }
                        it.isNetworkError() -> {
                            passwordConfirmationProgress.goneIf(false)
                            confirmResetPasswordButton.goneIf(true)
                            inputPasswordEditTextConfirmPassword.error = getString(R.string.Login_LoginFailedDueToNetworkProblemTryAgain)
                            enableViews(true)
                        }
                        else -> {
                            passwordConfirmationProgress.goneIf(false)
                            confirmResetPasswordButton.goneIf(true)
                            inputPasswordEditTextConfirmPassword.error = getString(R.string.Login_LoginAndPasswordWereNotRecognizedTryAgain)
                            enableViews(true)
                        }
                    }
                }
        )
    }
    private fun enableViews(enable: Boolean) {
        inputLoginEditTextConfirmPassword.isEnabled = enable
        inputPasswordEditTextConfirmPassword.isEnabled = enable
    }
}