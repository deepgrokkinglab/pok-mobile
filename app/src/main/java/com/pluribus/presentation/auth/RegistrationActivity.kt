package com.pluribus.presentation.auth

import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.pluribus.R
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.main.support.GuestSupportActivity
import com.pluribus.tools.hideKeyboard
import kotlinx.android.synthetic.main.activity_register.*

class RegistrationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        pager.isSaveEnabled = false
        pager.adapter = RegistrationAdapter(supportFragmentManager, lifecycle)
        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                hideKeyboard()
                tabLayout.selectTab(tabLayout.getTabAt(position))
            }
        })
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.currentItem = tab.position
                hideKeyboard()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        supportTextView.text = getString(R.string.support)
        supportTextView.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }

        btn_back.setOnClickListener { onBackPressed() }
    }

}
