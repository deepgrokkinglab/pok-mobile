package com.pluribus.presentation.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import androidx.appcompat.content.res.AppCompatResources
import com.pluribus.R
import com.pluribus.data.network.isNetworkError
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.support.GuestSupportActivity
import com.pluribus.presentation.start.StartActivity
import com.pluribus.tools.goneIf
import com.pluribus.tools.longToastRes
import com.pluribus.tools.validatePassword
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.generic.instance
import retrofit2.HttpException

class AuthActivity : BaseActivity() {

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, AuthActivity::class.java))
        }
    }

    private val authRepository by kodein.instance<AuthRepository>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar?.hide()

        forgotPasswordTextView.setOnClickListener {
            startActivity(Intent(this, ResetPasswordActivity::class.java))
        }

        logInButton.setOnClickListener {
            login()
        }

        registerButton.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }

        supportTextView.text = getString(R.string.support)
        supportTextView.setOnClickListener {
            GuestSupportActivity.startActivity(this)
        }


        inputPasswordEditText.transformationMethod = PasswordTransformationMethod()
        password_visiblity.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.design_ic_visibility))
        password_visiblity.setOnClickListener {
            val (newTransMethod, newVisibilityIcon) = if (inputPasswordEditText.transformationMethod == null) {
                PasswordTransformationMethod() to R.drawable.design_ic_visibility
            } else {
                null to R.drawable.design_ic_visibility_off
            }
            inputPasswordEditText.transformationMethod = newTransMethod
            password_visiblity.setImageDrawable(AppCompatResources.getDrawable(this, newVisibilityIcon))
        }
    }

    private fun login() {
        if (!validatePassword(inputPasswordEditText)) {
            return
        }
        enableViews(false)

        authProgress.goneIf(false)
        logInButton.goneIf(true)

        executeCompletable(
            authRepository.login(
                inputLoginEditText.text.toString(),
                inputPasswordEditText.text.toString()
            ),
            onComplete = {
                StartActivity.startClear(this)
            },
            onError = {
                authProgress.goneIf(true)
                logInButton.goneIf(false)
                enableViews(true)
                val errorStringRes = when {
                    (it as? HttpException)?.code() == 500 -> {
                        R.string.Login_ServerInternalErrorTryAgain
                    }
                    (it as? HttpException)?.code() == 461 -> {
                        R.string.Login_ResetPassword_UserNotFound
                    }
                    it.isNetworkError() -> {
                        R.string.Login_LoginFailedDueToNetworkProblemTryAgain
                    }
                    else -> {
                        R.string.Login_LoginAndPasswordWereNotRecognizedTryAgain
                    }
                }
                longToastRes(errorStringRes)
            }
        )
    }

    private fun enableViews(enable: Boolean) {
        inputLoginEditText.isEnabled = enable
        inputPasswordEditText.isEnabled = enable
    }

}