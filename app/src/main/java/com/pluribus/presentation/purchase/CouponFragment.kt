package com.pluribus.presentation.purchase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pluribus.BuildConfig
import com.pluribus.R
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.kodein
import com.pluribus.tools.hideKeyboard
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_coupons.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.kodein.di.generic.instance

class CouponFragment : Fragment() {

    private val wsRepository by kodein.instance<WSRepository>()

    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_coupons, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        wsRepository.requestSubscriptionState(BuildConfig.ROOM_ID, BuildConfig.BRAND)

        btn_activate.setOnClickListener {
            wsRepository.sendCoupon(input_coupon_code.text.toString(), BuildConfig.ROOM_ID, BuildConfig.BRAND)

            requireContext().toast(R.string.generic_pleaseWaitALittle_button)
            activity?.hideKeyboard()
        }

        disposable = wsRepository
            .observeCoupon
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.data.success) {
                    wsRepository.requestSubscriptionState(BuildConfig.ROOM_ID, BuildConfig.BRAND)
                    requireContext()
                        .longToast(resources.getString(R.string.Subscription_couponActivated_popup) + "\r\n${it.data.code}")
                } else {
                    requireContext()
                        .longToast(resources.getString(R.string.Subscription_errorCouponWasNotActivated_popup) + "\r\n${it.data.code}")
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable?.dispose()
        disposable = null
    }
}
