package com.pluribus.presentation.base

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_META_DATA
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.pluribus.domain.repositories.SettingsRepository
import com.pluribus.presentation.kodein
import com.pluribus.tools.setContextLocale
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.kodein.di.generic.instance


abstract class BaseActivity : AppCompatActivity() {

    companion object {
        private val ioScheduler = Schedulers.single()
        private val uiScheduler = AndroidSchedulers.mainThread()
    }

    private var disposables: CompositeDisposable? = null

    private var initialLang: String? = null

    protected val settingsRepository by kodein.instance<SettingsRepository>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialLang = settingsRepository.getLanguage()
        setContextLocale(this, settingsRepository.getLanguage())
        resetActivityTitle(this)
        disposables = CompositeDisposable()
    }

    override fun onResume() {
        super.onResume()
        if (initialLang != settingsRepository.getLanguage()) {
            finish()
            startActivity(intent)
            return
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables?.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(setContextLocale(base, settingsRepository.getLanguage()))
    }

    private fun resetActivityTitle(activity: Activity) {
        try {
            val pm = activity.packageManager
            val info = pm.getActivityInfo(activity.componentName, GET_META_DATA)
            if (info.labelRes != 0) {
                activity.setTitle(info.labelRes)
            } else {
                // по той же логике, как и в манифесте:
                // если тайтл активити явно не задан, то используем тайтл приложения.
                val appInfo = pm.getApplicationInfo(activity.packageName, GET_META_DATA)
                if (appInfo.labelRes != 0) {
                    activity.setTitle(appInfo.labelRes)
                }
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.toString()
        }
    }

    protected fun <T> executeObservable(
        observable: Observable<T>,
        onNext: (T) -> Unit = {},
        onComplete: () -> Unit = {},
        onError: (throwable: Throwable) -> Unit = {}
    ) {
        observable
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)
            .subscribe(onNext, onError, onComplete)
            .also { disposables?.add(it) }
    }

    protected fun executeCompletable(
        completable: Completable,
        onComplete: () -> Unit = {},
        onError: (throwable: Throwable) -> Unit = {}
    ) {
        completable
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)
            .subscribe(onComplete, onError)
            .also { disposables?.add(it) }
    }
}