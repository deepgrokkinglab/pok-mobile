package com.pluribus.presentation

import android.content.Context
import com.pluribus.BuildConfig
import com.pluribus.PluribusApplication
import com.pluribus.R
import com.pluribus.data.network.AuthApi
import com.pluribus.data.network.UserApi
import com.pluribus.data.network.LogApi
import com.pluribus.data.network.ServerApi
import com.pluribus.data.repository.*
import com.pluribus.data.repository.ws.WSRepositoryImpl
import com.pluribus.domain.interactors.AuthInteractor
import com.pluribus.domain.interactors.LogInteractor
import com.pluribus.domain.interactors.UserInteractor
import com.pluribus.domain.repositories.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val appModule = Kodein.Module("app") {
    bind<Context>() with provider { PluribusApplication.application }
}

val dataModule = Kodein.Module("data") {
    bind<Retrofit>() with singleton {
        val urlResId = if (Locale.getDefault().language == "zh") {
            if (BuildConfig.DEBUG) {
                R.string.china_host_dev
            } else {
                R.string.china_host_prod
            }
        } else {
            if (BuildConfig.DEBUG) {
                R.string.europe_host_dev
            } else {
                R.string.europe_host_prod
            }
        }
        val client = OkHttpClient
                .Builder()
                .connectTimeout(7, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .pingInterval(10, TimeUnit.SECONDS)
                .build()

        Retrofit.Builder()
                .client(client)
                .baseUrl(instance<Context>().getString(urlResId))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
    bind<AuthApi>() with provider { instance<Retrofit>().create(AuthApi::class.java) }
    bind<ServerApi>() with provider { instance<Retrofit>().create(ServerApi::class.java) }
    bind<ServerRepository>() with provider { ServerRepositoryImpl(instance()) }
    bind<AuthRepository>() with provider { AuthRepositoryImpl(instance(), instance()) }
    bind<UserApi>() with provider { instance<Retrofit>().create(UserApi::class.java) }
    bind<UserRepository>() with provider { UserRepositoryImpl(instance()) }
    bind<SettingsRepository>() with singleton { SettingsRepositoryImpl(instance()) }
    bind<WSRepository>() with singleton {

        val rtmApiSslUrlResId = if (BuildConfig.DEBUG) {
            R.string.rtm_api_ssl_url_dev
        } else {
            R.string.rtm_api_ssl_url_prod
        }

        val rtmApiUnsafeUrlResId = if (BuildConfig.DEBUG) {
            R.string.rtm_api_unsafe_url_dev
        } else {
            R.string.rtm_api_unsafe_url_prod
        }

        val ctx = instance<Context>()

        WSRepositoryImpl(ctx.getString(rtmApiSslUrlResId), ctx.getString(rtmApiUnsafeUrlResId), instance(), instance())
    }
    bind<LogApi>() with provider { instance<Retrofit>().create(LogApi::class.java) }
    bind<LogRepository>() with provider { LogRepositoryImpl(instance()) }

}

val domainModule = Kodein.Module("domain") {
    bind<AuthInteractor>() with provider { AuthInteractor(instance(), instance(), instance()) }
    bind<LogInteractor>() with provider { LogInteractor(instance(), instance()) }
    bind<UserInteractor>() with provider { UserInteractor(instance()) }
}


val kodein = Kodein {
    import(appModule)
    import(dataModule)
    import(domainModule)
}