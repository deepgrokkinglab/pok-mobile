package com.pluribus.presentation.main.settings

import android.annotation.SuppressLint
import android.os.Bundle
import com.pluribus.R
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.tools.configureActionBarForPluribusPoker
import kotlinx.android.synthetic.main.activity_languages.*

class LocaleActivity : BaseActivity() {

    companion object {
        val ENGLISH = 0
        val CHINESE = 1

        @SuppressLint("SetTextI18n")
        val LanguageOptions = sortedMapOf("en" to "English", "zh" to "Chinese")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_languages)

        languageSwitchEnglish.setOnClickListener {
            selectLanguage(ENGLISH)
        }

        languageSwitchChinese.setOnClickListener {
            selectLanguage(CHINESE)
        }
        configureActionBarForPluribusPoker()
    }

    private fun selectLanguage(langIndex: Int) {
        val oldLangCode = settingsRepository.getLanguage()
        val oldLangIndex = LanguageOptions.keys.toMutableList().indexOf(oldLangCode)

        if (oldLangIndex == langIndex)
            return

        val newLangCode = LanguageOptions.keys.toMutableList()[langIndex]
        settingsRepository.saveLanguage(newLangCode)
        recreate()
    }
}
