package com.pluribus.presentation.main.root.enableview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

abstract class EnableHintsPresenter {
    protected lateinit var view: View
    abstract val layoutRes: Int

    fun inflateLayout(inflater: LayoutInflater, parent: ViewGroup) {
        val inflatedView = inflater.inflate(layoutRes, parent, true)
        view = inflatedView
        onInflated(view)
    }

    abstract fun onInflated(view: View)

    abstract fun invalidateEnabledState(hintsEnabled: Boolean)
    abstract fun invalidateSubState(subscriptionActive: Boolean)
    abstract fun areHintsEnabled(active: Boolean)
    abstract fun presentWarnings(warnings: String, imageToId: Map<String, Int>)
}