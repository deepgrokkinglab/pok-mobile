package com.pluribus.presentation.main.settings

data class ServerData(val name: String,val ip: String, var isCurrentServer: Boolean, val ping: Int)