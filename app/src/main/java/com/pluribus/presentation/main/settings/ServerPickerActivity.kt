package com.pluribus.presentation.main.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.pluribus.R
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.domain.repositories.ServerRepository
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.tools.countPingToResource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_server_picker.*
import org.kodein.di.generic.instance
import timber.log.Timber
import java.net.URL

class ServerPickerActivity : BaseActivity() {

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, ServerPickerActivity::class.java))
        }
    }

    private val serverRepository by kodein.instance<ServerRepository>()
    private val wsRepository by kodein.instance<WSRepository>()
    private val authRepository by kodein.instance<AuthRepository>()

    private val adapter = ServerListAdapter()
    val disposables = CompositeDisposable()
    private val serversList = mutableListOf<ServerData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_picker)

        recyclerViewServersList.layoutManager = LinearLayoutManager(this)
        recyclerViewServersList.adapter = adapter

        val adapterDisposable = adapter.itemPositionClicksObservable.subscribe {
            val selectedServer = adapter.dataList[it]
            if (!selectedServer.isCurrentServer)
                useServer(selectedServer)
        }
        disposables.add(adapterDisposable)
        adapter.dataList = serversList

        imageArrowBackServerPicker.setOnClickListener {
            onBackPressed()
        }
        getServerList()
    }

    private fun getServerList() {
        val serverObservable = serverRepository.getServers()
                .flatMapObservable { Observable.fromIterable(it.servers) }
                .map {
                    ServerData(it.description, it.ip, false, 0)
                }
        val serverDisposable = serverObservable.withLatestFrom(wsRepository.observeHost) { server, latestHost ->
            if (latestHost.ip.contains(server.ip))
                server.copy(isCurrentServer = true)
            else server
        }.map {
            it.copy(ping = URL("http://${it.ip}").countPingToResource())
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (serversList.indexOf(it) == -1)
                        serversList.add(it)
                    adapter.notifyDataSetChanged()
                }, { error ->
                    Timber.e(error.localizedMessage)
                })

        disposables.add(serverDisposable)
    }

    private fun useServer(server: ServerData) {
        val userDataDisposable = authRepository.userData(authRepository.getUserToken()
                ?: "", settingsRepository.getUseProxy())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->

                    val isCurrentServer = serversList[serversList.indexOf(server)].isCurrentServer
                    serversList.filter {
                        it.isCurrentServer
                    }.forEach {
                        it.isCurrentServer = false
                    }
                    serversList[serversList.indexOf(server)].isCurrentServer = !isCurrentServer //= !server.isCurrentServer
                    adapter.notifyDataSetChanged()

                    wsRepository.stop()
                    wsRepository.connect(result.woken, result.proxy, server.ip)
                }, { error ->
                    Timber.e(error.localizedMessage)
                })

        disposables.add(userDataDisposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

}