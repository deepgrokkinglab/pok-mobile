package com.pluribus.presentation.main.settings

import android.graphics.Typeface
import android.os.Bundle
import android.widget.SeekBar
import com.pluribus.R
import com.pluribus.data.models.TextStyle
import com.pluribus.data.models.applyTo
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.tools.configureActionBarForPluribusPoker
import kotlinx.android.synthetic.main.activity_text_style.*

private const val TEXT_SIZE_SEEK_BAR_OFFSET = 10
private val STYLES = arrayOf(Typeface.NORMAL, Typeface.BOLD, Typeface.ITALIC, Typeface.BOLD_ITALIC)

class TextStyleActivity : BaseActivity(), SeekBar.OnSeekBarChangeListener {

    private lateinit var textAppearance: TextStyle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_style)

        textAppearance = settingsRepository.getTextStyle()

        seekBarTextSize.setOnSeekBarChangeListener(this)
        seekBarTextStyle.setOnSeekBarChangeListener(this)
        colorPicker.addColorObserver {
            userChangedSettings()
        }
        configureActionBarForPluribusPoker()
    }

    override fun onResume() {
        super.onResume()

        textAppearance.applyTo(tvExample)

        seekBarTextSize.progress = textAppearance.textSize.toInt()
        seekBarTextStyle.progress = STYLES.indexOf(textAppearance.typeface).coerceAtLeast(0)
        colorPicker.color = textAppearance.textColor
    }

    private fun userChangedSettings() {
        textAppearance = TextStyle(
            colorPicker.color,
            seekBarTextSize.progress.toFloat() + TEXT_SIZE_SEEK_BAR_OFFSET,
            STYLES[seekBarTextStyle.progress.coerceAtLeast(0).coerceAtMost(STYLES.size - 1)]
        )
        settingsRepository.saveTextStyle(textAppearance)

        textAppearance.applyTo(tvExample)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        if (fromUser) {
            userChangedSettings()
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }
}
