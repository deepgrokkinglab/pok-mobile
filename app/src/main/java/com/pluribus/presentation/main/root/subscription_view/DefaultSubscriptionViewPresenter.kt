package com.pluribus.presentation.main.root.subscription_view

import android.view.View
import com.pluribus.R
import kotlinx.android.synthetic.main.app_update_available.view.*
import kotlinx.android.synthetic.main.layout_subscription_view_default.view.*

class DefaultSubscriptionViewPresenter(private val onBalancePurchaseClicked: () -> Unit) : SubscriptionViewPresenter() {

    override val layoutRes = R.layout.layout_subscription_view_default

    override fun onInflated(view: View) {
        view.balanceHeader.text = view.context.resources.getString(R.string.subscription)
        view.limitTextView.visibility = View.VISIBLE
        view.buyButtonText.text = view.context.resources.getString(R.string.buy_subscription)
        view.buyButton.setOnClickListener { onBalancePurchaseClicked.invoke() }
    }

    override fun invalidateSubState(subscriptionActive: Boolean) {
        if (!subscriptionActive) {
            view.limitTextView.visibility = View.GONE
            view.progress.max = 100
            view.progress.progress = 100
            view.tv_trial.visibility = View.GONE
        } else {
            view.tv_trial.visibility = View.VISIBLE
            view.limitTextView.visibility = View.VISIBLE
        }
    }

    override fun setSubscriptionProgress(progress: Int) {
        view.progress.progress = progress
    }

    override fun setMaximumProgress(maxProgress: Int) {
        view.progress.max = maxProgress
    }


    override fun setLimits(limits: String) {
        view.limitTextView.text = limits
    }

    override fun setBalance(balance: String) {
        view.balanceValue.text = balance
    }

    override fun displayUpdatePrompt(text: String, onClick: ()-> Unit) {
        view.balanceHeader.visibility = View.GONE
        view.app_update_available.visibility = View.VISIBLE
        view.app_update_available.updateAvailableLabel.text = text
        view.app_update_available.setOnClickListener {
           onClick.invoke()
        }
    }
}