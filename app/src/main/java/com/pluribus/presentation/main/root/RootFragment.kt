package com.pluribus.presentation.main.root

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.pluribus.BuildConfig
import com.pluribus.R
import com.pluribus.data.models.AppVerModel
import com.pluribus.data.models.CouponSubscriptionModel
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.root.enableview.EnableHintsPresenterProvider
import com.pluribus.presentation.main.root.subscription_view.SubscriptionViewPresenterProvider
import com.pluribus.presentation.purchase.PurchaseActivity
import com.pluribus.presentation.record.LabelsService
import com.pluribus.presentation.system.DeviceOrientationManager
import com.pluribus.tools.compareCurrentAppVersionTo
import com.pluribus.tools.goneIf
import com.pluribus.tools.isAppInstalled
import com.pluribus.tools.openUrl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.app_update_available.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.kodein.di.generic.instance
import kotlin.math.roundToInt

class RootFragment : Fragment(R.layout.fragment_home) {

    companion object {
        fun newInstance(): RootFragment {
            return RootFragment()
        }
    }

    private val wsRepository by kodein.instance<WSRepository>()

    private val deviceOrientationManager by lazy { DeviceOrientationManager(requireContext()) }

    private var disposables: CompositeDisposable? = null

    private var enablePresenter = EnableHintsPresenterProvider().createEnableHintsPresenter(::onStartClicked)
    private var balancePresenter = SubscriptionViewPresenterProvider().createSubscriptionViewPresenter { PurchaseActivity.startActivity(requireContext()) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        enablePresenter.inflateLayout(inflater, view.layout_enable_hints)
        balancePresenter.inflateLayout(inflater, view.topSection)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wsRepository.observeConnection.doOnNext { connected ->
            if (connected) {
                wsRepository.requestSubscriptionState(BuildConfig.ROOM_ID, BuildConfig.BRAND)
            }
        }.subscribe()
                .also { disposables?.add(it) }
        addWarnings()
    }

    override fun onStart() {
        super.onStart()
        disposables = CompositeDisposable()
        wsRepository
                .observeSubscription
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    handleSubscription(it)
                }
                .also { disposables!!.add(it) }

        wsRepository.observeUpdate.observeOn(AndroidSchedulers.mainThread())
                .subscribe { updateData ->
                    checkShouldDisplayUpdatePrompt(updateData)
                }
                .also { disposables!!.add(it) }
    }

    private fun checkShouldDisplayUpdatePrompt(appVersionModel: AppVerModel) {
        val isNewerVersionAvailable = compareCurrentAppVersionTo(appVersionModel.data.version)
        if (isNewerVersionAvailable) {
            balancePresenter.displayUpdatePrompt(resources.getString(R.string.Main_updateAvailable_text, appVersionModel.data.version)) {
                context?.openUrl(appVersionModel.data.url)
            }
        }
    }

    private fun addWarnings() {
        val imagesWithIds: MutableMap<String, Int> = mutableMapOf()
        imagesWithIds["image-android-warning"] = R.drawable.ic_android_version_warning
        imagesWithIds["image-orientation-warning"] = R.drawable.ic_device_orientation_warning
        imagesWithIds["image-resolution-warning"] = R.drawable.ic_android_version_warning

        val wm = context!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)

        val sb = StringBuilder().apply {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
                append("image-android-warning ")
                appendLine(getString(R.string.android_version_unavailable))
                append("\n")
            }

            if (deviceOrientationManager.isDeviceDefaultOrientationLandscape()) {
                append("image-orientation-warning ")
                appendLine(getString(R.string.device_orientation_unavailable))
                append("\n")
            } else if (size.x < 760 || size.y < 1260) {
                append("image-resolution-warning ")
                appendLine(getString(R.string.device_low_resolution))
            }
        }

        if (sb.toString().isNotBlank())
            enablePresenter.presentWarnings(sb.toString(), imagesWithIds)
    }

    override fun onResume() {
        super.onResume()
        invalidateEnabledView()
        wsRepository.requestSubscriptionState(BuildConfig.ROOM_ID, BuildConfig.BRAND)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) {
            invalidateEnabledView()
            wsRepository.requestSubscriptionState(BuildConfig.ROOM_ID, BuildConfig.BRAND)
        }
    }

    override fun onStop() {
        super.onStop()
        disposables?.dispose()
        disposables = null
    }

    @SuppressLint("StringFormatMatches")
    private fun handleSubscription(subscription: CouponSubscriptionModel) {
        fun daysNumberFromMs(milliseconds: Long): Int {
            return (milliseconds.toFloat() / (60 * 60 * 24 * 1000).toFloat()).roundToInt()
        }

        val localTime = System.currentTimeMillis()
        var subscriptionActive = false
        val desc = when {
            subscription.data.paidTill > localTime -> {
                subscriptionActive = true
                resources
                        .getString(
                                R.string.Main_paidSubscriptionDescription_textLabel,
                                daysNumberFromMs(subscription.data.paidTill - localTime)
                        )
            }
            else -> {
                resources.getString(R.string.Main_subscriptionIsOff_textLabel)
            }
        }
        if (!subscriptionActive) {
            balancePresenter.setBalance(desc)
        } else {
            balancePresenter.setBalance(desc)
            balancePresenter.setLimits(resources.getString(
                    R.string.Main_paidSubscriptionDescription_textLabelLimit,
                    subscription.data.maxLimitEnc / 2,
                    subscription.data.maxLimitEnc
            ))
            val maxProgress = daysNumberFromMs(subscription.data.paidTill - subscription.data.activatedAt)
            balancePresenter.setMaximumProgress(maxProgress)
            balancePresenter.setSubscriptionProgress(maxProgress - daysNumberFromMs(subscription.data.paidTill - localTime))
        }
        balancePresenter.invalidateSubState(subscriptionActive)
        enablePresenter.invalidateSubState(subscriptionActive)
    }

    private fun onStartClicked() {
        val hintsOn = LabelsService.serviceAlive(requireContext())
        if (hintsOn) {
            LabelsService.stopService(requireContext())
        } else {
            val pokerAppPackage = getString(R.string.poker_app_package)
            if (requireContext().isAppInstalled(pokerAppPackage)) {
                RootOkFragment().show(requireActivity().supportFragmentManager, "A")
            } else {
                RootNeedInstallFragment().show(requireActivity().supportFragmentManager, "A")
            }
        }
        invalidateEnabledView()
    }

    private fun invalidateEnabledView() {
        enablePresenter.invalidateEnabledState(LabelsService.serviceAlive(requireContext()))
    }
}
