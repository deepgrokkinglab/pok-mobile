package com.pluribus.presentation.main.settings

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.pluribus.R
import com.pluribus.tools.setColor
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.layout_server_item.view.*


class ServerListAdapter : RecyclerView.Adapter<ServerListAdapter.ViewHolder>() {

    private val _itemPositionClicksObservable: PublishSubject<Int> = PublishSubject.create()
    val itemPositionClicksObservable: Observable<Int> =  _itemPositionClicksObservable

    var dataList: List<ServerData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val msgView = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.layout_server_item, parent, false)
        return ViewHolder(msgView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.setData(dataList[position])
        viewHolder.itemView.setOnClickListener {
            _itemPositionClicksObservable.onNext(position)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun setData(data: ServerData) {
            view.textViewServerNameItem.text = data.name

            val color = when {
                data.ping in 0..150 -> { R.color.green1 }
                data.ping in 150..300 -> { R.color.yellow1 }
                data.ping > 300 -> { R.color.red1 }
                else -> R.color.white
            }
            val msText = view.context.getString(R.string.Servers_servers_ping_ms, data.ping)
            val pingText = view.context.getString(R.string.Servers_servers_ping)
            val message = pingText + msText
            view.textViewServerPingItem.text = message
            view.textViewServerPingItem.setColor(message, msText, ContextCompat.getColor(view.context, color))
            view.switchServerItem.isChecked = data.isCurrentServer


        }
    }
}