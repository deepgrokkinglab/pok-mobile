package com.pluribus.presentation.main.root.subscription_view

import com.pluribus.BuildConfig

class SubscriptionViewPresenterProvider {
    fun createSubscriptionViewPresenter(onBalancePurchaseClicked: () -> Unit): SubscriptionViewPresenter {
        return when (BuildConfig.BRAND) {
            BuildConfig.BRAND_PLURIBUS, BuildConfig.BRAND_PIXUI, BuildConfig.BRAND_POKERTIME -> DefaultSubscriptionViewPresenter(onBalancePurchaseClicked)
            BuildConfig.BRAND_CAISEN -> NzdSubscriptionViewPresenter(onBalancePurchaseClicked)
            else -> throw IllegalStateException("createSubscriptionViewPresenter: wrong BuildConfig configuration")
        }
    }
}