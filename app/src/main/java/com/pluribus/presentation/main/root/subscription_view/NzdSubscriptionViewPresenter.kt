package com.pluribus.presentation.main.root.subscription_view

import android.view.View
import com.pluribus.R
import kotlinx.android.synthetic.main.app_update_available.view.*
import kotlinx.android.synthetic.main.layout_subscription_view_nzd.view.*

class NzdSubscriptionViewPresenter(private val onBalancePurchaseClicked: () -> Unit): SubscriptionViewPresenter() {
    override val layoutRes = R.layout.layout_subscription_view_nzd

    override fun onInflated(view: View) {
        view.balanceHeader.text = view.context.resources.getString(R.string.subscription)
        view.limitTextView.visibility = View.VISIBLE
        view.buyButton.setOnClickListener { onBalancePurchaseClicked.invoke() }    }

    override fun invalidateSubState(subscriptionActive: Boolean) {

    }

    override fun setSubscriptionProgress(progress: Int) {

    }

    override fun setMaximumProgress(maxProgress: Int) {

    }

    override fun setLimits(limits: String) {
        view.limitTextView.text = limits
    }

    override fun setBalance(balance: String) {
        view.balanceValue.text = balance
    }

    override fun displayUpdatePrompt(text: String, onClick: ()-> Unit) {
        view.app_update_available.visibility = View.VISIBLE
        view.app_update_available.updateAvailableLabel.text = text
        view.app_update_available.setOnClickListener {
            onClick.invoke()
        }
    }

}