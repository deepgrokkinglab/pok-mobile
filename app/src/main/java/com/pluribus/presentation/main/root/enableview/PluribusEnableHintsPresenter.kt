package com.pluribus.presentation.main.root.enableview

import android.view.View
import androidx.core.content.ContextCompat
import com.pluribus.R
import com.pluribus.tools.addImage
import com.pluribus.tools.invisibleIf
import kotlinx.android.synthetic.main.layout_enable_hints_pluribus.view.*

class PluribusEnableHintsPresenter(private val onStartClicked: () -> Unit) : EnableHintsPresenter() {
    override val layoutRes = R.layout.layout_enable_hints_pluribus

    override fun onInflated(view: View) {
        view.startButton.setOnClickListener {
            onStartClicked.invoke()
            view.startButton.isEnabled = false
            view.startButton.post { view.startButton.isEnabled = true }
        }
    }

    override fun invalidateEnabledState(hintsEnabled: Boolean) {
        view.startButton.isEnabled = true
        if (hintsEnabled) {
            view.startButton.setImageResource(R.drawable.hints_on)
            view.tv_hints.text = view.context.getString(R.string.hints_on)
            view.tv_hints.setTextColor(ContextCompat.getColor(view.context, R.color.yellow3))
        } else {
            view.startButton.setImageResource(R.drawable.hints_off)
            view.tv_hints.text = view.context.getString(R.string.hints_off)
            view.tv_hints.setTextColor(ContextCompat.getColor(view.context, R.color.grey6))
        }
    }

    override fun invalidateSubState(subscriptionActive: Boolean) {
        view.startButton.invisibleIf(!subscriptionActive)
        view.tv_hints.invisibleIf(!subscriptionActive)
    }

    override fun areHintsEnabled(active: Boolean) {
        if (!active) {
            view.startButton.setImageResource(R.drawable.hints_off)
            view.startButton.setOnClickListener(null)
        }
    }

    override fun presentWarnings(warnings: String, imageToId: Map<String, Int>) {
        view.tv_hints.visibility = View.GONE
        view.tv_hints.text = warnings
        imageToId.forEach {
            view.tv_hints.addImage(it.key, it.value, 64,64)
        }
    }
}