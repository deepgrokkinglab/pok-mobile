package com.pluribus.presentation.main.root.enableview

import com.pluribus.BuildConfig
import java.lang.IllegalStateException

class EnableHintsPresenterProvider {
    fun createEnableHintsPresenter(onStartClicked: () -> Unit) : EnableHintsPresenter {
        return when (BuildConfig.BRAND) {
            BuildConfig.BRAND_PLURIBUS -> PluribusEnableHintsPresenter(onStartClicked)
            BuildConfig.BRAND_PIXUI -> PixuiEnableHintsPresenter(onStartClicked)
            BuildConfig.BRAND_POKERTIME -> PixuiEnableHintsPresenter(onStartClicked)
            BuildConfig.BRAND_CAISEN -> NzdEnableHintsPresenter(onStartClicked)
            else -> throw IllegalStateException("createEnableHintsPresenter: wrong BuildConfig configuration")
        }
    }
}