package com.pluribus.presentation.main.root.subscription_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class SubscriptionViewPresenter {
    protected lateinit var view: View
    abstract val layoutRes: Int

    fun inflateLayout(inflater: LayoutInflater, parent: ViewGroup) {
        val inflatedView = inflater.inflate(layoutRes, parent, true)
        view = inflatedView
        onInflated(view)
    }

    abstract fun onInflated(view: View)

    abstract fun invalidateSubState(subscriptionActive: Boolean)
    abstract fun setSubscriptionProgress(progress: Int)
    abstract fun setMaximumProgress(maxProgress: Int)
    abstract fun setLimits(limits: String)
    abstract fun setBalance(balance: String)
    abstract fun displayUpdatePrompt(text: String, onClick: ()-> Unit)

}