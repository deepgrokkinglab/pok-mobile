package com.pluribus.presentation.main.root

import android.app.Dialog
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.Nullable
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pluribus.R
import com.pluribus.presentation.record.RecordingActivity
import kotlinx.android.synthetic.main.fragment_home_bottom_sheet_ok.*

class RootOkFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_bottom_sheet_ok, container, false)
    }

    override fun onCreateDialog(@Nullable savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialogInterface ->
                val bottomSheetDialog = dialogInterface as BottomSheetDialog?
                val bottomSheet: FrameLayout? = bottomSheetDialog
                    ?.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(bottomSheet!!)
                val layoutParams: ViewGroup.LayoutParams = bottomSheet.layoutParams
                val point = Point()
                requireActivity().windowManager.defaultDisplay.getRealSize(point)
                val windowHeight = point.y
                layoutParams.height = windowHeight
                bottomSheet.layoutParams = layoutParams
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonLaunch.setOnClickListener {
            RecordingActivity.startActivity(requireContext())
            dismiss()
        }
    }

}