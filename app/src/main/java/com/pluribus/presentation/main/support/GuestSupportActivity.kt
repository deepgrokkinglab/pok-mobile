package com.pluribus.presentation.main.support

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.pluribus.R
import com.pluribus.data.models.ContactsData
import com.pluribus.domain.interactors.UserInteractor
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.tools.configureActionBarForPluribusPoker
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_guest_support.*
import org.kodein.di.generic.instance

class GuestSupportActivity : BaseActivity() {

    companion object {

//        //Igor 2CLp824rPkB | 2HL29eczXNJ
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/INTUhxslsp44pMNbSbOMnYc",
//                name = "伊戈尔",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Arina 2CLownZooqw | 2HL3d1vvAwK
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/IBBpSOVXhRgasRKL0OWtMPw",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Olya 2CLosJGQq6P | 2HLgdjxSZ6s
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MJJSOz1lCvo75NDIVyGy6FA",
//                name = "李娅",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Lexa 2CbwQ9XacCA | 2HLgkBqUSUe
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/IOMZ91l42mQDC0fAO87AnUY",
//                name = "莱希兄弟",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Sonya 2HLwTMPMSY2 | 2HLrN6FFjCP
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MD5-AW62JW8upe1BOxazBRc",
//                name = "白阮",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Achapkinastya 2CLpFgXpaCP | 2HLgraJYtse
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MFvcP9GBvWZkwn3K4OwEk58",
//                name = "客服联系方式",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //RogerP 2FbAmWMN3S9 | 2HLhKQSwwCz
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MCwK7vDkjfsDlXVBQ6XwIo4",
//                name = "RogerP",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Yubao 2CcboE1tupt | 2HLh8KzpYGi
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/EBbIqyuR2nFPril7XKbsEy4",
//                name = "客服联系方式",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Xiaoshitou 2Ccbxj2CPMH | 2HLh1oE5F4e
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MEwv0ExL8aE0Nnc_spmyIYg",
//                name = "客服联系方式",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Pokerking 2Ccc6Sj7zSz | 2HLhrHJBfnY
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MIdBY2WagVw7oz3CmU9G3KU",
//                name = "Pokerking",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //ChinaAI 2Ccc6Sj7zSz | 2HLhYHSfi2r
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MNZIwO23apCV0tMKVu6Pjfw",
//                name = "China A.I.",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //tgpokerch 2Gpg7ovFt2P | 2HLhSGP6V8F
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MPVxNvUUThKwe-9vFbPNbAk",
//                name = "客服",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Shan 2CccGdNW6AB | 2HLhwogtT4N
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/EIPSGGEDhx2MT9_gyL-c318",
//                name = "Shan",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Bin bin 2CcoR8w7J74 evans1
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/EFbk--Z_iVnbWaBQxaBsQs8",
//                name = "Bin Bin",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Tuan 2Cd22A3bPDV | 2HLi4osatfX
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/ELSBhOMvZrkzwpl04DGgcH0",
//                name = "Tuan",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Luo 2Cp6VXX2qoE | 2HLiKUxsSvN
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MB4sdu70WgmSRw2ScT8Fk3M",
//                name = "Luo",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
        //Confucius 2HLgK6fdfEH | 2HLiDvzp3vW
        val defaultContact = ContactsData(
                type = 3,
                url = "https://u.wechat.com/MNK1DQxUpxlBwvGtquM1vSs",
                name = "客服中心",
                langs = emptyList(),
                scopes = emptyList()
        )
//        //Touge 2HbE7up1fYm |
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/INDijwSLBx3P7Di4iLCAmDM",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Kefu
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MKo7Kd8FCHUTHfPwWMSv_yw",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Liulian
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MEjBKJ77vkD5jvwnfAuYgwI",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Meng
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MHR83ytm9MGiUQZmNVtMDck",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //David
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MNo10Th1RquAbj3xhcfL-Yg",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Bobus
//        val defaultContact = ContactsData(
//                type = 3,
//                url = "https://u.wechat.com/MHNdUq0fYhJIx5KZZ_2U04A",
//                name = "客服中心",
//                langs = emptyList(),
//                scopes = emptyList()
//        )
//        //Null
//        val defaultContact = ContactsData(
//                type = 0,
//                url = "",
//                name = "",
//                langs = emptyList(),
//                scopes = emptyList()
//        )

        fun startActivity(context: Context) {
            context.startActivity(Intent(context, GuestSupportActivity::class.java))
        }
    }

    private val userInteractor by kodein.instance<UserInteractor>()

    private val adapter = ContactListAdapter()

    private lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest_support)
        configureActionBarForPluribusPoker()

        contactsRecycler.layoutManager = GridLayoutManager(this, 2)
        contactsRecycler.adapter = adapter

        disposable = userInteractor.getContacts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe({ contactsModel ->
                    adapter.dataList = listOf(defaultContact)
//                    adapter.dataList = contactsModel.data
                }, { error ->
                    adapter.dataList = listOf(defaultContact)
                    println(error.localizedMessage)
                })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

}
