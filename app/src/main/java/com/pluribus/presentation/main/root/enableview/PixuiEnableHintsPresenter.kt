package com.pluribus.presentation.main.root.enableview

import android.view.View
import android.widget.CompoundButton
import com.pluribus.R
import com.pluribus.tools.addImage
import com.pluribus.tools.invisibleIf
import kotlinx.android.synthetic.main.layout_enable_hints_pixui.view.*

class PixuiEnableHintsPresenter(private val onStartClicked: () -> Unit) : EnableHintsPresenter() {
    override val layoutRes = R.layout.layout_enable_hints_pixui

    override fun onInflated(view: View) {
        view.switch_enable_app.setOnCheckedChangeListener(::onCheckedChanged)
        view.enable_layout.setOnClickListener { view.switch_enable_app.toggle() }
    }

    override fun invalidateEnabledState(hintsEnabled: Boolean) {
        view.switch_enable_app.setOnCheckedChangeListener(null)
        view.switch_enable_app.isChecked = hintsEnabled
        view.switch_enable_app.setOnCheckedChangeListener(::onCheckedChanged)
    }

    override fun invalidateSubState(subscriptionActive: Boolean) {
        view.enable_layout.invisibleIf(!subscriptionActive)
    }

    override fun areHintsEnabled(active: Boolean) {
        view.switch_enable_app.isEnabled = active
        if(!active) {
            view.tv_hints_pixui.visibility = View.VISIBLE
        }
    }

    override fun presentWarnings(warnings: String, imageToId: Map<String, Int>) {
        view.tv_hints_pixui.visibility = View.VISIBLE
        view.tv_hints_pixui.text = warnings
        imageToId.forEach {
            view.tv_hints_pixui.addImage(it.key, it.value, 64,64)
        }
    }

    private fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        onStartClicked.invoke()
    }
}