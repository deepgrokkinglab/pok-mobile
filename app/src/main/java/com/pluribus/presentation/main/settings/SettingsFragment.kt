package com.pluribus.presentation.main.settings

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.pluribus.BuildConfig
import com.pluribus.R
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.domain.repositories.SettingsRepository
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.kodein
import com.pluribus.presentation.record.LabelsService
import com.pluribus.presentation.start.StartActivity
import kotlinx.android.synthetic.main.fragment_more.*
import kotlinx.android.synthetic.main.fragment_more.view.*
import org.jetbrains.anko.toast
import org.kodein.di.generic.instance

class SettingsFragment : Fragment(R.layout.fragment_more) {

    companion object {
        val LanguageOptions = sortedMapOf("en" to "English", "zh" to "Chinese")

        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }

    private val authRepository by kodein.instance<AuthRepository>()
    private val settingsRepository by kodein.instance<SettingsRepository>()
    private val wsRepository by kodein.instance<WSRepository>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvAppVersion.text = BuildConfig.VERSION_NAME
        tvAppVersionIncremental.text = resources.getString(R.string.Settings_appVersionNameIncremental, BuildConfig.VERSION_CODE)
        btnLogout.setOnClickListener {
            authRepository.logout()
            wsRepository.stop()
            LabelsService.stopService(requireContext())
            StartActivity.startClear(requireContext())
        }

        tvUserLogin.setOnClickListener {
            val text = (it as TextView).text.toString()
            val context = view.context
            (context.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager)
                ?.setPrimaryClip(ClipData.newPlainText("text", text))
            context.toast(context.resources.getString(R.string.generic_didCopy_arg, text))
        }

        hintTextAppearanceLayout.setOnClickListener {
            startActivity(Intent(requireContext(), TextStyleActivity::class.java))
        }

        serverChangeLayout.setOnClickListener {
            ServerPickerActivity.startActivity(requireContext())
        }

        languageSwitchLayout.setOnClickListener {
            startActivity(Intent(requireContext(), LocaleActivity::class.java))
        }

        tapToUseProxy.isChecked = settingsRepository.getUseProxy()
        tapToUseProxy.setOnCheckedChangeListener { buttonView, isChecked ->
            settingsRepository.setUseProxy(isChecked)
        }
        useProxyLayout.setOnClickListener {
            view.tapToUseProxy.toggle()
        }

        curLang.text = LanguageOptions[settingsRepository.getLanguage()]
        tvUserLogin.text = settingsRepository.getUserName()
    }

}
