package com.pluribus.presentation.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.pluribus.BuildConfig
import com.pluribus.R
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.root.RootFragment
import com.pluribus.presentation.main.settings.SettingsFragment
import com.pluribus.presentation.main.support.SupportFragment
import com.pluribus.presentation.record.LabelsService
import com.pluribus.presentation.start.StartActivity
import com.pluribus.presentation.system.DisconnectLabelsManager
import com.pluribus.tools.setSingleItemIconSizeRes
import kotlinx.android.synthetic.main.activity_tabs.*
import org.kodein.di.generic.instance

class MainActivity : BaseActivity() {

    companion object {
        private val menuIdToFragments: Map<Int, Fragment> = mapOf(
                R.id.bottom_nav_menu_action_support to SupportFragment.newInstance(),
                R.id.bottom_nav_menu_action_main to RootFragment.newInstance(),
                R.id.bottom_nav_menu_action_more to SettingsFragment.newInstance()
        )

        fun startActivity(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }

        fun startClear(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }

    private val wsRepo by kodein.instance<WSRepository>()
    private lateinit var labelsManager: DisconnectLabelsManager

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tabs)

        labelsManager = DisconnectLabelsManager(this)

        selectTab(R.id.bottom_nav_menu_action_main)

        navigationView.setOnNavigationItemSelectedListener(
                object : NavigationView.OnNavigationItemSelectedListener,
                        BottomNavigationView.OnNavigationItemSelectedListener {
                    override fun onNavigationItemSelected(item: MenuItem): Boolean {
                        val fragment = menuIdToFragments[item.itemId] ?: return false
                        navigateTo(fragment)
                        return true
                    }
                }
        )
        executeObservable(
                observable = wsRepo.observeConnection,
                onNext = { connected ->
                    if (!LabelsService.serviceAlive(this)) {
                        labelsManager.showDisconnect(!connected)
                    }
                }
        )

        executeObservable(observable = wsRepo.observeLogout, onNext = { shouldLogout ->
            if (shouldLogout) {
                Toast.makeText(this, getString(R.string.Notification_login_from_another_device), Toast.LENGTH_LONG).show()
                StartActivity.startClear(this)
            }
        })

        when (BuildConfig.BRAND) {
            BuildConfig.BRAND_PIXUI, BuildConfig.BRAND_POKERTIME -> {
                navigationView.setSingleItemIconSizeRes(
                        1,
                        R.dimen.bottom_navigation_big_icon_size
                )
            }
            BuildConfig.BRAND_CAISEN -> {
                navigationView.setSingleItemIconSizeRes(
                        1,
                        R.dimen.bottom_navigation_huge_icon_size
                )
                navigationView.itemIconTintList = null
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        labelsManager.showDisconnect(false)
    }

    fun navigateTo(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }

    private fun selectTab(menuId: Int) {
        navigationView.selectedItemId = menuId
        navigateTo(menuIdToFragments[menuId] ?: error(""))
    }

}