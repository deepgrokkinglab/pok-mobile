package com.pluribus.presentation.main.support

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.pluribus.R
import com.pluribus.data.models.ContactsData
import com.pluribus.tools.QRGenerator
import com.pluribus.tools.openUrl

class ContactListAdapter : RecyclerView.Adapter<ContactListAdapter.ViewHolder>() {

    var dataList: List<ContactsData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val msgView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.contact_view_in_list, parent, false)
        return ViewHolder(msgView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.setData(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun setData(data: ContactsData) {
            view.findViewById<ImageView>(R.id.messengerIcon).setImageResource(getContactTypeIcon(data.type))
            view.findViewById<TextView>(R.id.contactText).text = data.name
            view.setOnClickListener {
                showContactQrCode(view.context, data)
            }
        }

        private fun showContactQrCode(context: Context, contact: ContactsData) {
            val bitmap = QRGenerator.generateBitmap(context, 2, contact.url)

            val view = LayoutInflater.from(context).inflate(R.layout.qrcode_with_url, null, false)
            view.findViewById<ImageView>(R.id.image).setImageBitmap(bitmap)

            val dialog = AlertDialog.Builder(context)
                .setTitle(contact.name)
                .setView(view)
                .setNeutralButton(R.string.generic_open) { _, _ ->
                    context.openUrl(contact.url)
                }
                .setPositiveButton(R.string.generic_copy) { _, _ ->
                    (context.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager)
                        ?.setPrimaryClip(ClipData.newPlainText("text", contact.url))
                }
                .create()

            dialog.show()
        }

        private fun getContactTypeIcon(type: Int): Int {
            return when (type) {
                1 -> R.drawable.ic_tg
                2 -> R.drawable.ic_chat
                3 -> R.drawable.ic_chat
                else -> R.drawable.ic_black_180
            }
        }
    }
}