package com.pluribus.presentation.main.support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.pluribus.BuildConfig
import com.pluribus.R
import com.pluribus.domain.repositories.WSRepository
import com.pluribus.presentation.kodein
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_support.*
import org.kodein.di.generic.instance

class SupportFragment : Fragment() {

    companion object {
        fun newInstance(): SupportFragment {
            return SupportFragment()
        }
    }

    private val wsRepository by kodein.instance<WSRepository>()

    private var disposable: Disposable? = null
    private val adapter = ContactListAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_support, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        contactsRecycler.layoutManager = GridLayoutManager(context, 2)
        contactsRecycler.adapter = adapter
        adapter.dataList = listOf(GuestSupportActivity.defaultContact)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable?.dispose()
        disposable = null
    }
}
