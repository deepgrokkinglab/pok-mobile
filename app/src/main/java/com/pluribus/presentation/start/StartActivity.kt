package com.pluribus.presentation.start

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import com.pluribus.AuthLandingActivity
import com.pluribus.BuildConfig
import com.pluribus.BuildConfig.BRAND_CAISEN
import com.pluribus.R
import com.pluribus.data.network.isNetworkError
import com.pluribus.domain.interactors.AuthInteractor
import com.pluribus.presentation.auth.AuthActivity
import com.pluribus.presentation.base.BaseActivity
import com.pluribus.presentation.kodein
import com.pluribus.presentation.main.MainActivity
import com.pluribus.tools.PermissionsHelper
import com.pluribus.tools.SwipeRecentDetectorService
import org.kodein.di.generic.instance
import retrofit2.HttpException

class StartActivity : BaseActivity() {

    companion object {

        private const val REQUEST_CODE_OVERLAY_PERMISSION = 5000

        /**
         * Открыть StartActivity предварительно закрыв все остальные activity.
         */
        fun startClear(context: Context) {
            val intent = Intent(context, StartActivity::class.java)
            intent.flags =
                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }

    private val authInteractor by kodein.instance<AuthInteractor>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        checkState()
        startService(Intent(this, SwipeRecentDetectorService::class.java))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_OVERLAY_PERMISSION) {
            checkState()
        }
    }

    private fun checkState() {
        when {
            !PermissionsHelper.checkOverlayPermission(applicationContext) -> {
                AlertDialog
                    .Builder(this)
                    .setTitle(R.string.AllowPermission_pleaseAllowPermission_caption)
                    .setMessage(R.string.AllowPermission_youMustAllowThePermissionToRunAppProperly_text)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        startActivityForResult(
                            Intent(
                                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:${applicationContext.packageName}")
                            ),
                            REQUEST_CODE_OVERLAY_PERMISSION
                        )
                    }
                    .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                        dialog.dismiss()
                        finish()
                    }
                    .show()
            }
            !authInteractor.isAuthorized() -> {
                if (BuildConfig.BRAND == BRAND_CAISEN) {
                    AuthLandingActivity.startActivity(this)
                } else {
                    AuthActivity.startActivity(this)
                }
                finish()
            }
            else -> {
                start()
            }
        }
    }

    private fun start() {
        executeCompletable(
            completable = authInteractor.start(),
            onComplete = {
                MainActivity.startClear(this)
            },
            onError = {
                if ((it as? HttpException)?.code() == 500 || it.isNetworkError()) {
                    start()
                } else {
                    if (BuildConfig.BRAND == BRAND_CAISEN) {
                        AuthLandingActivity.startActivity(this)
                    } else {
                        AuthActivity.startActivity(this)
                    }
                    finish()
                }
            }
        )
    }

}
