package com.pluribus.domain.interactors

import com.pluribus.data.models.ContactsModel
import com.pluribus.domain.repositories.UserRepository
import io.reactivex.Single

class UserInteractor(
        private val userRepository: UserRepository
) {
    fun getContacts(): Single<ContactsModel> {
        return userRepository.getContacts()
    }
}