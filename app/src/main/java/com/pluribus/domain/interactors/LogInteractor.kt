package com.pluribus.domain.interactors

import com.pluribus.data.models.UploadLogsRequest
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.domain.repositories.LogRepository
import com.pluribus.tools.files.FileHelper
import com.pluribus.tools.files.FileZipper
import io.reactivex.Completable
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody

class LogInteractor(
        private val authRepository: AuthRepository,
        private val logRepository: LogRepository
) {

    fun uploadLogs(): Completable {
        val file = FileHelper.generateFile("logfile.txt")
        val zipFile = FileHelper.generateFile("logs.zip")
        val logFilePath = arrayOf(file!!.absolutePath)
        FileZipper.zipFiles(logFilePath, zipFile!!.absolutePath)

        val requestFile = zipFile.readBytes().toRequestBody("application/zip".toMediaType())
        val partFile = MultipartBody.Part.createFormData("logs", zipFile.name, requestFile)

        return logRepository.uploadLogs(UploadLogsRequest(authRepository.getUserToken()
                ?: "", partFile))
    }

}