package com.pluribus.domain.interactors

import com.pluribus.data.models.ConfirmResetPasswordRequest
import com.pluribus.data.models.ResetPasswordRequest
import com.pluribus.domain.repositories.AuthRepository
import com.pluribus.domain.repositories.SettingsRepository
import com.pluribus.domain.repositories.WSRepository
import io.reactivex.Completable

class AuthInteractor(
    private val wsRepository: WSRepository,
    private val authRepository: AuthRepository,
    private val settingsRepository: SettingsRepository
) {
    fun isAuthorized(): Boolean {
        return authRepository.getUserToken() != null
    }

    fun start(): Completable {
        return if (wsRepository.started) {
            Completable.complete()
        } else {
            authRepository
                .userData(authRepository.getUserToken()!!)
                .flatMapCompletable {
                    Completable.fromAction {
                        wsRepository.start(it.woken)
                        settingsRepository.saveUserName(it.username)
                    }
                }
        }
    }

    fun confirmPasswordReset(password_reset_code: String, email: String, password: String): Completable {
        return authRepository.confirmResetUserPassword(ConfirmResetPasswordRequest(password_reset_code, email, password))
    }

    fun resetPassword(resetPasswordRequest: ResetPasswordRequest): Completable {
        return authRepository.resetUserPassword(resetPasswordRequest)
    }
}