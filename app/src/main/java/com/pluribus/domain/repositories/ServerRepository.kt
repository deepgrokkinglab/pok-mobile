package com.pluribus.domain.repositories

import com.pluribus.data.models.Server
import com.pluribus.data.models.ServerResult
import io.reactivex.Single

interface ServerRepository {
    fun getServers(): Single<ServerResult>
    fun getFastestServer():  Single<Server>
}