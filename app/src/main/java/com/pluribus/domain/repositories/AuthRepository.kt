package com.pluribus.domain.repositories

import com.pluribus.data.models.*
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {

    fun startRegistration(request: RegistrationRequest): Single<RegistrationResult>

    fun sendSms(request: SendSmsRequest): Completable

    fun confirmSms(request: ConfirmSmsRequest): Completable

    fun login(login: String, password: String): Completable

    fun userData(token: String, useProxy: Boolean = false): Single<UserDataResult>

    fun logout()

    fun getUserToken(): String?

    fun resetUserPassword(request: ResetPasswordRequest): Completable

    fun confirmResetUserPassword(request: ConfirmResetPasswordRequest): Completable
}