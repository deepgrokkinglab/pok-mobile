package com.pluribus.domain.repositories

import com.pluribus.data.models.UploadLogsRequest
import io.reactivex.Completable

interface LogRepository {

    fun uploadLogs(uploadLogsRequest: UploadLogsRequest): Completable

}