package com.pluribus.domain.repositories

import com.pluribus.data.models.ContactsModel
import io.reactivex.Single

interface UserRepository {

    fun getContacts(): Single<ContactsModel>
}