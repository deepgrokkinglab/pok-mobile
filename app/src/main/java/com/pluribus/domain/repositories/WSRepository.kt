package com.pluribus.domain.repositories

import com.pluribus.data.models.*
import io.reactivex.Observable

interface WSRepository {

    val started: Boolean

    val observeConnection: Observable<Boolean>

    val observeHelp: Observable<HelpViewModel>

    val observeCoupon: Observable<CouponViewModel>

    val observeSubscription: Observable<CouponSubscriptionModel>

    val observeContacts: Observable<ContactsModel>

    val observeLogout: Observable<Boolean>

    val observeUpdate: Observable<AppVerModel>

    val observeHost: Observable<Server>

    fun start(wsToken: String)

    fun stop()

    fun connect(wsToken: String, proxyModel: ProxyModel? = null, customHost: String? = null)


    fun sendScreenShot(screenShot: ByteArray, isBug: Boolean)

    fun sendCoupon(code: String, roomId: Int, brand: String)

    fun requestSubscriptionState(roomId: Int, brand: String)

    fun requestContacts()

    fun requestContacts(agentId: String, roomId : Int)

    fun sendStatistics(uuid: String)

    fun sendPendingRequests()
}