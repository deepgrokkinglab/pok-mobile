package com.pluribus.domain.repositories

import com.pluribus.data.models.TextStyle
import io.reactivex.Observable

interface SettingsRepository {

    fun saveUserName(userName: String)

    fun getUserName(): String

    fun saveLanguage(lang: String)

    fun getLanguage(): String

    fun saveTextStyle(textStyle: TextStyle)

    fun getTextStyle(): TextStyle

    fun getOptionalTextStyle(): TextStyle?

    fun setUseProxy(useProxy: Boolean)

    fun getUseProxy(): Boolean

    fun getUseProxyObservable(): Observable<Boolean>
}